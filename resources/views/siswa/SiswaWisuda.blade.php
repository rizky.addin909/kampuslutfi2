@extends('../layout/applog')

@section('title','Nama Website')


<!-- Page content -->
@section('content')
@foreach( $users as $user)
@if($user->id == auth()->user()->id)
  <!-- Header -->
  <?php $ns = 0; ?>
  <header class="w3-container" style="padding-top:22px">
     <h4><span>Daftar <strong>Yudisium</strong></span><br><h4>
  </header>

  <div class="w3-panel">
    <div class="w3-row-padding" style="margin:0 -16px">
      <div class="w3-twothird">
        <h5>Daftar Wisuda</h5>
        <div class="container">
  
</div>
        <table class="w3-table w3-striped w3-white">
          @foreach( $wisuds as $wisu)
          @foreach( $siswa as $sis)
          @if($sis->user_id == auth()->user()->id)
          @if($wisu->id_siswa == $sis->id)
              <form action="{{ route('prosesdaftarwisuda') }}" method="post" enctype="multipart/form-data">
                @csrf
                
                <tr>
                  <td>
                    <p>Status Pendaftaran : </p>
                    @foreach( $siswa as $sis)
                    @if($sis->user_id == auth()->user()->id)
                    @if($sis->status =='Proses')
                      <p>Menunggu konfirmasi dari kampus </p>
                    @else
                      <p>Data Sudah di konfirmasi silahkan ke kampus untuk melanjutkan </p>
                    @endif
                    @endif
                    @endforeach
                  </td>
                </tr>
                
                <br>
                
                <tr>
                  <td>
                    <p>Pesan Dari Admin : </p>
                    @if($wisu->komentar == null)
                      <p> </p>
                    @else
                      <p> {{$wisu->komentar}} </p>
                    @endif
                  </td>
                </tr>
                

                <tr>
                  <td>
                    <p>Email : </p>
                    <input class="uk-input uk-width-1-1 uk-margin-large-left form-control" type="text" placeholder="Alamat Email" name="email" value="{{ $wisu->email }}">
                  </td>
                </tr>

                <tr>
                  <td>
                    <p>Nama Lengkap : </p>
                    <input class="uk-input uk-width-1-3 uk-margin-large-left form-control" type="text" placeholder="Nama Depan" name="namlep" value="{{ $wisu->nama_lengkap }}" >
                  </td>
                </tr>

                <tr>
                  <td>
                    <p>Nim</p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left " type="text" placeholder="**.**.****" name="nim" value="{{ $wisu->nim ?: $user->nim}}" >
                  </td>
                </tr>

                <tr>
                  <td>
                    <p>Alamat Email</p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left form-control" type="text" placeholder="ex(@gmail.com)" name="alemail" value="{{ $wisu->alamat_email }}">
                  </td>
                </tr>

                <tr>
                <td>
                    <p>No Handphone (Masih Aktif)</p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left form-control " type="text" placeholder="08*******" name="nohp" value="{{ $wisu->nohp }}">
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Nama Orang Tua</p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left form-control " type="text" placeholder="Your answer" name="namortu" value="{{ $wisu->nama_ortu }}" >
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Program Studi</p>
                    <select class="uk-input uk-width-1-4 uk-margin-large-left form-control" name="prodi" id="prodi">
                      @if($wisu->prodi == null)
                            <option value="">Pilih Prodi</option>
                            <option value="Teknik Kimia">Teknik Kimia</option>
                            <option value="Teknik Mesin">Teknik Mesin</option>
                            <option value="Akuntansi">Akuntansi</option>
                            <option value="Budidaya Tanaman Perkebunan D3">Budidaya Tanaman Perkebunan D3</option>
                            <option value="Budidaya Tanaman Perkebunan D4">Budidaya Tanaman Perkebunan D4</option>
                    @else
                            <option value="{{$wisu->prodi}}">{{$wisu->prodi}}</option>
                            <option value="">Pilih Prodi</option>
                            <option value="Teknik Kimia">Teknik Kimia</option>
                            <option value="Teknik Mesin">Teknik Mesin</option>
                            <option value="Akuntansi">Akuntansi</option>
                            <option value="Budidaya Tanaman Perkebunan D3">Budidaya Tanaman Perkebunan D3</option>
                            <option value="Budidaya Tanaman Perkebunan D4">Budidaya Tanaman Perkebunan D4</option>
                  @endif
                            
                    </select>
                  </td>
                </tr>

                <tr>
                <td>
                    <p>IPK </p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left form-control " type="text" placeholder="Your answer" name="ipk" value="{{ $wisu->ipk_mahasiswa }}" >
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Tempat Lahir (Sesuai Kartu Keluarga)</p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left form-control" type="text" placeholder="Your answer" name="temla" value="{{ $wisu->tempat_lahir }}">
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Tanggal Lahir</p>
                      <div class='input-group date' id='datepicker'>
                        <input class="uk-input uk-width-1-4 uk-margin-large-left" type="text" placeholder="MM/DD/YYYY"  name="tangla" value="{{$wisu->tanggal_lahir}}" />
                        <span class="input-group-addon" style="display: inline;">
                            <span class="glyphicon glyphicon-calendar" ></span>
                        </span>
                      </div>                  
                    </td>
                </tr>

                <tr>
                  <td>
                    <p>Alamat Domisili (Asal) </p>
                    <textarea class="uk-input uk-width-1-4 uk-margin-large-left form-control" name="alamat"> {{ $wisu->alamat_domisili }} </textarea>
                  </td>
                </tr>

                <tr>
                <td>
                    <p>NIK (Nomor Induk Kependudukan)</p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left form-control" type="text" placeholder="Your answer" name="nik" value="{{ $wisu->nik }}" >
                  </td>
                </tr>
                
                <input class="uk-input uk-width-1-1 uk-margin-large-left" type="hidden" name="sisid" value="{{ $wisu->id_siswa }}">


                <tr>
                <td>
                    <p>Tempat PKL I </p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left form-control" type="text" placeholder="Your answer" name="tempat_pkl1" value="{{ $wisu->tempat_pkl1 }}" >
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Tanggal Pelaksanaan I </p>
                    <div class='input-group date' id='datepicker1'>
                        <input class="uk-input uk-width-1-4 uk-margin-large-left" type="text" placeholder="MM/DD/YYYY"  name="tanggal_pelaksanaan" value="{{ $wisu->tanggal_pelaksanaan }}" />
                        <span class="input-group-addon" style="display: inline;">
                            <span class="glyphicon glyphicon-calendar" ></span>
                        </span>
                      </div>
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Judul PKL I </p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left form-control" type="text" placeholder="Your answer" name="judul_pkl1" value="{{ $wisu->judul_pkl1 }}" >
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Tempat PKL II </p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left form-control" type="text" placeholder="Your answer" name="tempat_pkl2" value="{{ $wisu->tempat_pkl2 }}" >
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Tanggal Pelaksanaan II </p>
                    <div class='input-group date' id='datepicker2'>
                        <input class="uk-input uk-width-1-4 uk-margin-large-left" type="text" placeholder="MM/DD/YYYY"  name="tanggal_pelaksanaan2" value="{{ $wisu->tanggal_pelaksanaan2 }}" />
                        <span class="input-group-addon" style="display: inline;">
                            <span class="glyphicon glyphicon-calendar" ></span>
                        </span>
                      </div>
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Judul PKL II </p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left form-control" type="text" placeholder="Your answer" name="judul_pkl2" value="{{ $wisu->judul_pkl2 }}" >
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Tempat PKL III </p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left form-control" type="text" placeholder="Your answer" name="tempat_pkl3" value="{{ $wisu->tempat_pkl3 }}" >
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Tanggal Pelaksanaan III </p>
                    <div class='input-group date' id='datepicker3'>
                        <input class="uk-input uk-width-1-4 uk-margin-large-left" type="text" placeholder="MM/DD/YYYY"  name="tanggal_pelaksanaan3" value="{{ $wisu->tanggal_pelaksanaan3 }}" />
                        <span class="input-group-addon" style="display: inline;">
                            <span class="glyphicon glyphicon-calendar" ></span>
                        </span>
                      </div>
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Judul PKL III </p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left form-control" type="text" placeholder="Your answer" name="judul_pkl3" value="{{ $wisu->judul_pkl3 }}" >
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Judul Tugas Akhir </p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left form-control" type="text" placeholder="Your answer" name="judul_tugas_akhir" value="{{ $wisu->judul_tugas_akhir }}" >
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Nama Pembimbing Tugas Akhir (1) </p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left form-control" type="text" placeholder="Your answer" name="nama_pembimbing_tugas_akhir1" value="{{ $wisu->nama_pembimbing_tugas_akhir1 }}" >
                  </td>
                </tr>
                <tr>
                <td>
                    <p>Nama Pembimbing Tugas Akhir (2) </p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left form-control" type="text" placeholder="Your answer" name="nama_pembimbing_tugas_akhir2" value="{{ $wisu->nama_pembimbing_tugas_akhir2 }}" >
                  </td>
                </tr>
                <tr>
                <td>
                    <p>Nama Pembimbing Tugas Akhir (3) </p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left form-control" type="text" placeholder="Your answer" name="nama_pembimbing_tugas_akhir3" value="{{ $wisu->nama_pembimbing_tugas_akhir3 }}" >
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Nama Penguji Tugas Akhir (1) </p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left form-control" type="text" placeholder="Your answer" name="nama_penguji_tugas_akhir1" value="{{ $wisu->nama_penguji_tugas_akhir1 }}" >
                  </td>
                </tr>
                <tr>
                <td>
                    <p>Nama Penguji Tugas Akhir (2) </p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left form-control" type="text" placeholder="Your answer" name="nama_penguji_tugas_akhir2" value="{{ $wisu->nama_penguji_tugas_akhir2 }}" >
                  </td>
                </tr>
                <tr>
                <td>
                    <p>Nama Penguji Tugas Akhir (3) </p>
                    <input class="uk-input uk-width-1-4 uk-margin-large-left form-control" type="text" placeholder="Your answer" name="nama_penguji_tugas_akhir3" value="{{ $wisu->nama_penguji_tugas_akhir3 }}" >
                  </td>
                </tr>


                <tr>
                <td>
                    <p>Upload Scan Asli Kartu Keluaga (file PDF)</p>
                    <input type="file" name="filekk" >
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Upload Scan Asli Akte Kelahiran (File PDF) *</p>
                    <input type="file" name="filekk2"  >
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Upload Foto Berwarna Background Putih</p>
                    <input type="file" name="filekk3"  >
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Upload Foto Berwarna Background Merah *</p>
                    <input type="file" name="filekk4" multiple="true" >
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Upload Ijazah SMA/SMK/MA/Paket C </p>
                    <input type="file" name="filekk5" multiple="true" >
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Upload Scan KTP Asli</p>
                    <input type="file" name="filekk6" multiple="true" >
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Upload Scan Bukti Pembayaran Biaya Wisuda</p>
                    <input type="file" name="filekk7" multiple="true" >
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Upload Scan Bukti Penyerahan KTM</p>
                    <input type="file" name="filekk8" multiple="true" >
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Upload Scan Lembar Pengesahan Tugas Akhir </p>
                    <input type="file" name="filekk9" multiple="true" >
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Upload Scan Sertifikat PPK (Program Pengenalan Kampus)</p>
                    <input type="file" name="filekk10" multiple="true" >
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Upload Scan Bukti Penyerahan Laporan PKL I, II, III dan Proyek Akhir</p>
                    <input type="file" name="filekk11" multiple="true" >
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Upload Scan Bukti Bebas Tanggungan Pustaka (Politeknik LPP)</p>
                    <input type="file" name="filekk12" multiple="true" >
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Upload Scan Bukti Bebas Tanggungan Administrasi (Bagian Keuangan)</p>
                    <input type="file" name="filekk13" multiple="true" >
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Upload Scan Surat Keterangan mengikuti Kegiatan Wajib Non-Akademik Bela Diri</p>
                    <input type="file" name="filekk14" multiple="true" >
                  </td>
                </tr>

                <tr>
                  <td>
                    <input type="submit" value="Kirim">
                  </td>
                </tr>
              </form>
          @endif
          @endif
          @endforeach
          @endforeach
          
        </table>
      </div>
    </div>
  </div>

  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
  <script >
      $(function () {
          $('#datepicker').datepicker({
              format: "dd/mm/yyyy",
              autoclose: true,
              todayHighlight: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            autoclose: true,
            changeMonth: true,
            changeYear: true,
            orientation: "button"
          });
      });

      $(function () {
          $('#datepicker1').datepicker({
              format: "dd/mm/yyyy",
              autoclose: true,
              todayHighlight: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            autoclose: true,
            changeMonth: true,
            changeYear: true,
            orientation: "button"
          });
      });

      $(function () {
          $('#datepicker2').datepicker({
              format: "dd/mm/yyyy",
              autoclose: true,
              todayHighlight: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            autoclose: true,
            changeMonth: true,
            changeYear: true,
            orientation: "button"
          });
      });

      $(function () {
          $('#datepicker3').datepicker({
              format: "dd/mm/yyyy",
              autoclose: true,
              todayHighlight: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            autoclose: true,
            changeMonth: true,
            changeYear: true,
            orientation: "button"
          });
      });
  </script>

  @endif
  @endforeach
  @endsection

  @section('footer')
  <!-- Footer -->
  <footer class="w3-container w3-padding-16 w3-light-grey">
    <center><p>Copyright © 1994 - 2020 LPP YOGYAKARTA. </p></center>
  </footer>
@endsection
