  <!DOCTYPE html>
<html>
<head>
<title>Politeknik Perkebunan Yogyakarta</title>
<link rel="shortcut icon" href="image/background/poster-lpp.png" type="image/png">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
html,body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}

</style>


  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker3.css" rel="stylesheet" id="bootstrap-css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>




</head>

<body class="w3-light-grey">
    <!-- Top container -->
<div class="w3-bar w3-top w3-grey w3-large" style="z-index:4">
  <button class="w3-bar-item w3-button w3-hide-large w3-hover-none w3-hover-text-light-grey" onclick="w3_open();"><i class="fa fa-bars"></i>  Menu</button>
  <span class="w3-bar-item w3-left"> <img src="/image/background/poster-lpp.png" height="40px" width="250px"> </span>
  <span class="w3-bar-item w3-right"><a class="dropdown-item" style="color: white;" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><button class="btn bgm-red" style="background-color: #f44336;">
                                        {{ __('Logout') }}
                                    </button></a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form></span>
</div>

<!-- Sidebar/menu -->
<nav class="w3-sidebar w3-collapse w3-white w3-animate-left" style="z-index:3;width:300px;" id="mySidebar"><br>
  <div class="w3-container w3-row">
    <div class="w3-col s4">
      @foreach( $users as $user)
      @if($user->id == auth()->user()->id)
      <img src="/uploads/avatars/{{ $user->avatar }}" class="w3-circle w3-margin-right" style="width:86px" alt="">

      @endif
      @endforeach
    </div>
    @guest
    @else
    <div class="w3-col s8 w3-bar">
      @foreach( $users as $user)
        @if($user->id == auth()->user()->id)
          @if($user->status =='Mahasiswa')
            @foreach($siswas as $siswa)
              @if($siswa->user_id == auth()->user()->id)
                <br>
                <br>
                <center><span>Welcome, <strong>{{ Auth::user()->name }} {{$siswa->nama_belakang}}</strong></span></center><br>
              @endif
            @endforeach
          @endif
        @endif
      @endforeach
      
      @foreach( $users as $user)
        @if($user->id == auth()->user()->id)
          @if($user->status =='admin')
      	    <br>
            <br>
            <center><span>Welcome , <strong>{{ Auth::user()->name }}</strong></span><center><br>
          @endif
          @if($user->status =='adminsuper')
            <br>
            <br>
            <center><span>Welcome , <strong>{{ Auth::user()->name }}</strong></span><center><br>
          @endif
        @endif
      @endforeach
      
    </div>
    @endguest
  </div>
  <hr>
  <div class="w3-container">
  </div>
  <div class="w3-bar-block">
    <a href="#" class="w3-bar-item w3-button w3-padding-16 w3-hide-large w3-dark-grey w3-hover-black" onclick="w3_close()" title="close menu"><i class="fa fa-remove fa-fw"></i>  Close Menu</a>
    @foreach( $users as $user)
    @if($user->id == auth()->user()->id)
    @if($user->status =='admin')
    <a href="{{route('home')}}" class="w3-bar-item w3-button w3-padding w3-blue"><i class="fa fa-users fa-fw"></i>  Overview</a>
    <a href="{{ route('viewuser') }}" class="w3-bar-item w3-button w3-padding"><i class="fa fa-eye fa-fw"></i>  Data User </a>
    <a href="{{ route('viewwisuda') }}" class="w3-bar-item w3-button w3-padding"><i class="fa fa-eye fa-fw"></i>  Data Yudisium </a>
    <a href="{{ route('viewberita') }}" class="w3-bar-item w3-button w3-padding"><i class="fa fa-eye fa-fw"></i>  Data Berita </a>
    <a href="{{ route('viewbukualumni') }}" class="w3-bar-item w3-button w3-padding"><i class="fa fa-eye fa-fw"></i>  Buku Alumni </a>
    @endif
    @if($user->status =='adminsuper')
    <a href="{{route('home')}}" class="w3-bar-item w3-button w3-padding w3-blue"><i class="fa fa-users fa-fw"></i>  Overview</a>
    <a href="{{ route('viewusersuper') }}" class="w3-bar-item w3-button w3-padding"><i class="fa fa-eye fa-fw"></i>  Data User </a>
    <a href="{{ route('viewwisudasuper') }}" class="w3-bar-item w3-button w3-padding"><i class="fa fa-eye fa-fw"></i>  Data Yudisium </a>
    <a href="{{ route('viewberitasuper') }}" class="w3-bar-item w3-button w3-padding"><i class="fa fa-eye fa-fw"></i>  Data Berita </a>
    <a href="{{ route('viewbukualumni') }}" class="w3-bar-item w3-button w3-padding"><i class="fa fa-eye fa-fw"></i>  Buku Alumni </a>
    @endif
    @if($user->status =='Mahasiswa')
    <div class="dropdown">
    <a href="{{ route('siswaedit') }}" class="w3-bar-item w3-button w3-padding "><i class="fa fa-user fa-fw"></i>  Profile</a>
    <a href="{{route('home')}}" class="w3-bar-item w3-button w3-padding "><i class="fa fa-home fa-fw"></i>  Dashboard</a>
    <a href="#" class="w3-bar-item w3-button w3-padding dropdown-toggle" data-toggle="dropdown"><i class="fa fa-list fa-fw"></i>  Yudisium <span class="caret"></span></a>
    <div class=" dropdown-menu">
      @foreach($siswas as $siswa)
      @if($siswa->user_id == auth()->user()->id)
      <a href="{{route('storewisuda1', $siswa->id)}}" class="w3-bar-item w3-button w3-padding">Pendaftaran</a>
      @endif
      @endforeach
    </div>
    
  </div>
    
    @endif
    @endif
    @endforeach
  </div>
</nav>


<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

<!-- !PAGE CONTENT! -->
<div class="w3-main" style="margin-left:300px;margin-top:43px;">
    @yield('content')
    @yield('footer')
    </div>

<script>
// Get the Sidebar
var mySidebar = document.getElementById("mySidebar");

// Get the DIV with overlay effect
var overlayBg = document.getElementById("myOverlay");

// Toggle between showing and hiding the sidebar, and add overlay effect
function w3_open() {
  if (mySidebar.style.display === 'block') {
    mySidebar.style.display = 'none';
    overlayBg.style.display = "none";
  } else {
    mySidebar.style.display = 'block';
    overlayBg.style.display = "block";
  }
}

// Close the sidebar with the close button
function w3_close() {
  mySidebar.style.display = "none";
  overlayBg.style.display = "none";
}
</script>


    </body>
</html>
