@extends('../layout/applagi')

@section('title','Nama Website')


<!-- Page content -->
@section('content')
    <div class="login-section">
          <div class="login-w3l"> 
              <h2 class="sub-head-w3-agileits">Reset Password</h2> 
              <hr>   
              <div class="login-form">      
              @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">

                            <div class="col-md-6">
                                <input id="email" placeholder="{{ __('E-Mail Address') }}" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <br>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </div>
                    </form>
            </div>
      <!-- //login -->
          <div class="clear"></div>
          <hr>
            <div class="login-form"> 
                <p>Kembali untuk login jika ingat password akun anda Silahkan <a href="{{route('login')}}">klik disini</a> </p>
            </div> 
        </div>
        <div class="clear"></div>
    </div> 

@endsection

@section('footer')
<!-- Footer -->
    <p class="footer" style="color: black;">Copyright © 1994 - 2020 LPP YOGYAKARTA.</p>

@endsection
