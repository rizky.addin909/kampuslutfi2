@extends('../layout/applog')

@section('title','Nama Website')


<!-- Page content -->
@section('content')
  <!-- Header -->
  <header class="w3-container" style="padding-top:22px">
    <h4><span>Data <strong>User</strong></span><br><h4>
  </header>

  <div class="w3-panel">
    <div class="w3-row-padding" style="margin:0 -16px">
      <div class="w3-twothird">
        <h5>Feeds</h5>
        <table class="w3-table w3-striped w3-white">
          @foreach( $users as $user)
          @if($user->id == $id)
              <form action="{{route('edituserupdatesuper')}}" method="post">
                @csrf
                <input id="iduser" type="hidden"  name="iduser" value="{{$user->id}}">
                <p>Nama</p>
                <input type="text" name="username" id="username" value="{{$user->name}}">
                <p>Email</p>
                <input type="text" name="email" id="email" value="{{$user->email}}">
                <p>Status Akun</p>
                <select class="uk-input uk-width-1-4 uk-margin-large-left form-control" name="status" id="status">
                    <option value="{{$user->status}}">{{$user->status}}</option>
                    <option value="">Pilih Status User</option>
                    <option value="admin">Admin</option>
                    <option value="Mahasiswa">Mahasiswa</option>
                </select>
                @foreach( $siswas as $siswa)
                @if($siswa->user_id == $id)
                <p>Status Mahasiswa</p>
                <input type="text" name="stat" id="stat" value="{{$siswa->status}}">
                @endif
                @endforeach
                <input type="submit" value="ubah">
              </form>
          @endif
          @endforeach
          
        </table>
      </div>
    </div>
  </div>
  @endsection

  @section('footer')
  <!-- Footer -->
  <footer class="w3-container w3-padding-16 w3-light-grey">
    <p>Created by </p>
  </footer>
@endsection
