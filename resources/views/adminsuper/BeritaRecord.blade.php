@extends('../layout/applog')

@section('title','Nama Website')


<!-- Page content -->
@section('content')
  <!-- Header -->
  <header class="w3-container" style="padding-top:22px">
    <h4><span>Data <strong>Berita</strong></span><br><h4>
  </header>

  <div class="w3-panel">
    <div class="w3-row-padding" style="margin:0 -16px">
      <div class="w3-twothird">
        <h5>Feeds</h5>
        <a href="{{ route('tambahberitasuper') }}"><button> <i class="fa fa-eye fa-fw"></i>  Tambah Berita  </button></a>
        <table class="w3-table w3-striped w3-white">
          @foreach( $berita as $ber)
          <tr>
            <td>{{$nil ++}}</i></td>
            <td>{{$ber->berita}}</td>
            <td>
              <form action="{{ route('editberitasuper', $ber->id) }}" method="get">
                @csrf
                <input id="idberita" type="hidden"  name="idberita" value="{{$ber->id}}">
                <input type="submit" value="edit">
              </form>
            </td>
            <td>
              <form action="{{ route('hapusberitasuper', $ber->id) }}" method="post">
                @csrf
                <input id="idberita" type="hidden"  name="idberita" value="{{$ber->id}}">
                <input type="submit" value="hapus">
              </form>
            </td>
          </tr>

          @endforeach
          
        </table>
      </div>
    </div>
  </div>
  @endsection

  @section('footer')
  <!-- Footer -->
  <footer class="w3-container w3-padding-16 w3-light-grey">
    <center><p>Copyright © 1994 - 2020 LPP YOGYAKARTA. </p></center>
  </footer>
@endsection
