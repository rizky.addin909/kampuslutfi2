@extends('../layout/applog')

@section('title','Nama Website')


<!-- Page content -->
@section('content')
  <!-- Header -->
  <header class="w3-container" style="padding-top:22px">
    <h4><span>Data <strong>Berita</strong></span><br><h4>
  </header>

  <div class="w3-panel">
    <div class="w3-row-padding" style="margin:0 -16px">
      <div class="w3-twothird">
        <h5>Feeds</h5>
        <table class="w3-table w3-striped w3-white">
          @foreach( $berita as $ber)
          @if($ber->id == $id)
              <form action="{{route('editberitaupdatesuper')}}" method="post">
                @csrf
                <input id="berid" type="hidden"  name="berid" value="{{$ber->id}}">
                <p>isi berita : </p>
                <textarea name="berita" id="berita"> {{$ber->berita}} </textarea>
                <input type="submit" value="ubah">
              </form>
          @endif
          @endforeach
          
        </table>
      </div>
    </div>
  </div>
  @endsection

  @section('footer')
  <!-- Footer -->
  <footer class="w3-container w3-padding-16 w3-light-grey">
    <p>Created by </p>
  </footer>
@endsection
