@extends('../layout/applog')

@section('title','Nama Website')


<!-- Page content -->
@section('content')
  <!-- Header -->
  <header class="w3-container" style="padding-top:22px">
    <h4><span>Data <strong>Berita</strong></span><br><h4>
  </header>

  <div class="w3-panel">
    <div class="w3-row-padding" style="margin:0 -16px">
      <div class="w3-twothird">
        <h5>Feeds</h5>
        <table class="w3-table w3-striped w3-white">
              <form action="{{route('tambahberitaprosessuper')}}" method="post">
                @csrf
                <p>isi berita : </p>
                <textarea name="berita" id="berita"> </textarea>
                <br>
                <input type="submit" value="kirim">
              </form>
          
        </table>
      </div>
    </div>
  </div>
  @endsection

  @section('footer')
  <!-- Footer -->
  <footer class="w3-container w3-padding-16 w3-light-grey">
    <center><p>Copyright © 1994 - 2020 LPP YOGYAKARTA. </p></center>
  </footer>
@endsection
