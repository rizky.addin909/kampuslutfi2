<!DOCTYPE html>
<html>
<head>
  <title>Buku Alumni</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

  <style>
    @page {
                margin: 0cm 0cm;
            }

            /**
            * Define the real margins of the content of your PDF
            * Here you will fix the margins of the header and footer
            * Of your background image.
            **/
            body {
                margin-top:    2cm;
                margin-bottom: 1cm;
                margin-left:   2cm;
                margin-right:  2cm;
            }

    #watermark {
                position: fixed;
                bottom:   0px;
                left:     0px;

                /** Change image dimensions**/
                width:    210mm;
                height:   297mm;

                /** Your watermark should be behind every content**/
                z-index:  -1000;
            }
            #watermarks {
                position: fixed;

                /** 
                    Set a position in the page for your image
                    This should center it vertically
                **/
                bottom:   0px;
                left:     6.5cm;

                /** Change image dimensions**/
                width:    8cm;
                height:   2cm;

                /** Your watermark should be behind every content**/
                z-index:  -1000;
            }
    </style>

</head>
<body>
  <div id="watermark">
    <img src="image/background/layout.jpg" height="100%" width="100%" />
  </div>

  <div id="watermarks">
    <center><p>Buku Tahunan Politeknik LPP Tahun 2019</p></center>
    <center><p>Halaman</p></center>
  </div>

  <main> 
  <style type="text/css">
    table tr td,
    table tr th{
      font-size: 9pt;
    }
  </style>
  <style>
        table {
          border-collapse: collapse;
          border: 0px solid black;
        }
        th,td {
            font-size: 15px;
        }
          .garis_atas{
            border-bottom: 3px black solid;
            height: 5px;
            width: 0px;
          }
          .garis_bawah{
            border-bottom: 3px black solid;
            height: 5px;
            width: 0px;
          }
        tr:hover {background-color: lightblue;}
</style>

    <table align="center">
      <tr>
        <td colspan="4" style="font-size: 30px; text-align: center;"><p><b>KATA PENGANTAR</b></p></td>
      </tr>

      <tr>
        <td colspan="4" style="font-size: 17px; text-align: justify; text-indent: 60px;"><p>Assalamualaikum wr.wb.</p></td>
      </tr>
      <tr>
        <td colspan="4" style="font-size: 17px; text-align: justify; text-indent: 60px;"><p>Dengan mengucapkan puji dan syukur kehadirat Allah Yang Maha Pengasih dan Penyayang, Politeknik LPP diberi kesempatan dan kemudahan untuk melepaskan lulusan dalam acara wisuda ke-19 tahun 2019. Seluruh jajaran pengelola dan pelaksana mengucapkan <br>selamat dan sukses bagi seluruh wisudawan yangtelah menyelesaikan studi di Politeknik LPP.</p></td>
      </tr>
      <tr>
        <td colspan="4" style="font-size: 17px; text-align: justify; text-indent: 60px;"><p>Selanjutnya kami mengucapkan selamat berkarya dan men-gamalkan ilmu untuk kepentingan bangsa dan negara kita tercinta. Jadilah pribadi-pribadi yang jujur, ikhlas, mandiri, dan berkarakter ditengah masyarakat.</p></td>
      </tr>
      <tr>
        <td colspan="4" style="font-size: 17px; text-align: justify; text-indent: 60px;"><p>Do’a kami menyertai seluruh wisudawan/wati.</p></td>
      </tr>
      <tr>
        <td colspan="4" style="font-size: 17px; text-align: justify; text-indent: 60px;"><p>Wassalamu’alaikum wr.wb.</p></td>
      </tr>
    </table>

    <table align="left" style=" padding-top: 100%; ">
      <tr>
        <td colspan="4" style="font-size: 30px; text-align: center;"><p><b>DAFTAR ISI</b></p></td>
      </tr>

      <tr>
        <td colspan="4" style="font-size: 17px; text-align: justify;"><p><b>PENGANTAR</b>......................................................................................................... i</p></td>
      </tr>
      <tr>
        <td colspan="4" style="font-size: 17px; text-align: justify;"><p><b>DAFTAR ISI</b>........................................................................................................... ii</p></td>
      </tr>
      <tr>
        <td colspan="4" style="font-size: 17px; text-align: justify;"><p><b>Lulusan Terbaik</b>................................................................................................... 1</p></td>
      </tr>
      <tr>
        <td colspan="4" style="font-size: 17px; text-align: justify;"><p><b>D3 Teknik Kimia</b>................................................................................................... 1</p></td>
      </tr>
      <tr>
        <td colspan="4" style="font-size: 17px; text-align: justify;"><p><b>D3 Teknik Kimia</b>................................................................................................. 13</p></td>
      </tr>
      <tr>
        <td colspan="4" style="font-size: 17px; text-align: justify;"><p><b>D3 Akuntansi</b>...................................................................................................... 18</p></td>
      </tr>
      <tr>
        <td colspan="4" style="font-size: 17px; text-align: justify;"><p><b>D3 Budidaya Tanaman Perkebunan</b>................................................................. 21</p></td>
      </tr>
      <tr>
        <td colspan="4" style="font-size: 17px; text-align: justify;"><p><b>D4 Budidaya Tanaman Perkebunan</b>................................................................. 35</p></td>
      </tr>
    </table>
  
  <center>
    
    <table align="center" style=" padding-top: 100%; ">
      <tr>
        <td colspan="4" style="font-size: 30px; text-align: center;"><p><b>LULUSAN TERBAIK</b></p></td>
      </tr>
        <tr >
          <td colspan="4" class="garis_atas"> </td>
        </tr>
        <tr >
          <td colspan="4" style="padding-top: 10px;"> </td>
        </tr>
      @foreach($wisuda as $wisu)
      @foreach($siswas as $sis)
      @if($sis->status == 'Lulus')
      @if($sis->id == $wisu->id_siswa)
      @if($wisu->ipk_mahasiswa > '3.50')
        
        <tr>
            <th rowspan="10" style=""><center><img src="uploads/wisudas/{{$wisu->nama_lengkap}}/{{$wisu->file_bbm}}" style="margin-left: 15px; margin-right: 10px;  border: 3px solid black ; " width="140px" height="180"></th>
        </tr>
        <tr>
            <th style="padding-left: 15px;">Nama </th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nama_lengkap}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Nim</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nim}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Tempat Tanggal Lahir</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->tempat_lahir}}, {{$wisu->tanggal_lahir}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Alamat Asal</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->alamat_domisili}}</td>
        </tr>

        <tr >
            <th style="padding-left: 15px;">Nama Orangtua</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nama_ortu}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">No. Telp</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nohp}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Email</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->email}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Program Studi</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->prodi}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">IPK</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->ipk_mahasiswa}}</td>
        </tr>

        <tr >
          <td colspan="4" class="garis_bawah"> </td>
        </tr>
        <tr >
          <td colspan="4" style="padding-top: 10px;"> </td>
        </tr>
        @endif
        @endif
        @endif
        @endforeach
        @endforeach
    </table>
  </center>

  <center>
    <table align="center" style=" padding-top: 100%; ">
      <tr>
        <td colspan="4" style="font-size: 30px; text-align: center;"><p><b>D3 TEKNIK KIMIA</b></p></td>
      </tr>
        <tr >
          <td colspan="4" class="garis_atas"> </td>
        </tr>
        <tr >
          <td colspan="4" style="padding-top: 10px;"> </td>
        </tr>
      @foreach($wisuda as $wisu)
      @foreach($siswas as $sis)
      @if($sis->status == 'Lulus')
      @if($sis->id == $wisu->id_siswa)
      @if($wisu->prodi == 'Teknik Kimia')
      @if($wisu->ipk_mahasiswa <= '3.50')
        
        <tr>
            <th rowspan="10" style=""><center><img src="uploads/wisudas/{{$wisu->nama_lengkap}}/{{$wisu->file_bbm}}" style="margin-left: 15px; margin-right: 10px;  border: 3px solid black ; " width="140px" height="180"></th>
        </tr>
        <tr>
            <th style="padding-left: 15px;">Nama </th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nama_lengkap}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Nim</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nim}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Tempat Tanggal Lahir</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->tempat_lahir}}, {{$wisu->tanggal_lahir}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Alamat Asal</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->alamat_domisili}}</td>
        </tr>

        <tr >
            <th style="padding-left: 15px;">Nama Orangtua</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nama_ortu}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">No. Telp</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nohp}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Email</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->email}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Program Studi</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->prodi}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">IPK</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->ipk_mahasiswa}}</td>
        </tr>

        <tr >
          <td colspan="4" class="garis_bawah"> </td>
        </tr>
        <tr >
          <td colspan="4" style="padding-top: 10px;"> </td>
        </tr>
        @endif
        @endif
        @endif
        @endif
        @endforeach
        @endforeach
    </table>

  </center>

  <center>
    <table align="center" style=" padding-top: 100%; ">
      <tr>
        <td colspan="4" style="font-size: 30px; text-align: center;"><p><b>D3 TEKNIK MESIN</b></p></td>
      </tr>
        <tr >
          <td colspan="4" class="garis_atas"> </td>
        </tr>
        <tr >
          <td colspan="4" style="padding-top: 10px;"> </td>
        </tr>
      @foreach($wisuda as $wisu)
      @foreach($siswas as $sis)
      @if($sis->status == 'Lulus')
      @if($sis->id == $wisu->id_siswa)
      @if($wisu->prodi == 'Teknik Mesin')
      @if($wisu->ipk_mahasiswa <= '3.50')
        
        <tr>
            <th rowspan="10" style=""><center><img src="uploads/wisudas/{{$wisu->nama_lengkap}}/{{$wisu->file_bbm}}" style="margin-left: 15px; margin-right: 10px;  border: 3px solid black ; " width="140px" height="180"></th>
        </tr>
        <tr>
            <th style="padding-left: 15px;">Nama </th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nama_lengkap}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Nim</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nim}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Tempat Tanggal Lahir</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->tempat_lahir}}, {{$wisu->tanggal_lahir}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Alamat Asal</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->alamat_domisili}}</td>
        </tr>

        <tr >
            <th style="padding-left: 15px;">Nama Orangtua</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nama_ortu}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">No. Telp</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nohp}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Email</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->email}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Program Studi</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->prodi}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">IPK</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->ipk_mahasiswa}}</td>
        </tr>

        <tr >
          <td colspan="4" class="garis_bawah"> </td>
        </tr>
        <tr >
          <td colspan="4" style="padding-top: 10px;"> </td>
        </tr>
        @endif
        @endif
        @endif
        @endif
        @endforeach
        @endforeach
    </table>

  </center>

  <center>
    <table align="center" style=" padding-top: 100%; ">
      <tr>
        <td colspan="4" style="font-size: 30px; text-align: center;"><p><b>D3 AKUNTANSI</b></p></td>
      </tr>
        <tr >
          <td colspan="4" class="garis_atas"> </td>
        </tr>
        <tr >
          <td colspan="4" style="padding-top: 10px;"> </td>
        </tr>
      @foreach($wisuda as $wisu)
      @foreach($siswas as $sis)
      @if($sis->status == 'Lulus')
      @if($sis->id == $wisu->id_siswa)
      @if($wisu->prodi == 'Akuntansi')
      @if($wisu->ipk_mahasiswa <= '3.50')
        
        <tr>
            <th rowspan="10" style=""><center><img src="uploads/wisudas/{{$wisu->nama_lengkap}}/{{$wisu->file_bbm}}" style="margin-left: 15px; margin-right: 10px;  border: 3px solid black ; " width="140px" height="180"></th>
        </tr>
        <tr>
            <th style="padding-left: 15px;">Nama </th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nama_lengkap}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Nim</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nim}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Tempat Tanggal Lahir</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->tempat_lahir}}, {{$wisu->tanggal_lahir}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Alamat Asal</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->alamat_domisili}}</td>
        </tr>

        <tr >
            <th style="padding-left: 15px;">Nama Orangtua</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nama_ortu}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">No. Telp</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nohp}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Email</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->email}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Program Studi</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->prodi}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">IPK</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->ipk_mahasiswa}}</td>
        </tr>

        <tr >
          <td colspan="4" class="garis_bawah"> </td>
        </tr>
        <tr >
          <td colspan="4" style="padding-top: 10px;"> </td>
        </tr>
        @endif
        @endif
        @endif
        @endif
        @endforeach
        @endforeach
    </table>

  </center>

  <center>
    <table align="center" style=" padding-top: 100%; ">
      <tr>
        <td colspan="4" style="font-size: 30px; text-align: center;"><p><b>D3 BUDIDAYA TANAMAN <p><b>PERKEBUNAN</b></p></b></p></td>
      </tr>
        <tr >
          <td colspan="4" class="garis_atas"> </td>
        </tr>
        <tr >
          <td colspan="4" style="padding-top: 10px;"> </td>
        </tr>
      @foreach($wisuda as $wisu)
      @foreach($siswas as $sis)
      @if($sis->status == 'Lulus')
      @if($sis->id == $wisu->id_siswa)
      @if($wisu->prodi == 'Budidaya Tanaman Perkebunan D3')
      @if($wisu->ipk_mahasiswa <= '3.50')
        
        <tr>
            <th rowspan="10" style=""><center><img src="uploads/wisudas/{{$wisu->nama_lengkap}}/{{$wisu->file_bbm}}" style="margin-left: 15px; margin-right: 10px;  border: 3px solid black ; " width="140px" height="180"></th>
        </tr>
        <tr>
            <th style="padding-left: 15px;">Nama </th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nama_lengkap}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Nim</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nim}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Tempat Tanggal Lahir</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->tempat_lahir}}, {{$wisu->tanggal_lahir}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Alamat Asal</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->alamat_domisili}}</td>
        </tr>

        <tr >
            <th style="padding-left: 15px;">Nama Orangtua</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nama_ortu}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">No. Telp</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nohp}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Email</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->email}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Program Studi</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->prodi}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">IPK</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->ipk_mahasiswa}}</td>
        </tr>

        <tr >
          <td colspan="4" class="garis_bawah"> </td>
        </tr>
        <tr >
          <td colspan="4" style="padding-top: 10px;"> </td>
        </tr>
        @endif
        @endif
        @endif
        @endif
        @endforeach
        @endforeach
    </table>

  </center>

  <center>
    <table align="center" style=" padding-top: 100%; ">
      <tr>
        <td colspan="4" style="font-size: 30px; text-align: center;"><p><b>D4 BUDIDAYA TANAMAN <p><b>PERKEBUNAN</b></p></b></p></td>
      </tr>
        <tr >
          <td colspan="4" class="garis_atas"> </td>
        </tr>
        <tr >
          <td colspan="4" style="padding-top: 10px;"> </td>
        </tr>
      @foreach($wisuda as $wisu)
      @foreach($siswas as $sis)
      @if($sis->status == 'Lulus')
      @if($sis->id == $wisu->id_siswa)
      @if($wisu->prodi == 'Budidaya Tanaman Perkebunan D4')
      @if($wisu->ipk_mahasiswa <= '3.50')
        
        <tr>
            <th rowspan="10" style=""><center><img src="uploads/wisudas/{{$wisu->nama_lengkap}}/{{$wisu->file_bbm}}" style="margin-left: 15px; margin-right: 10px;  border: 3px solid black ; " width="140px" height="180"></th>
        </tr>
        <tr>
            <th style="padding-left: 15px;">Nama </th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nama_lengkap}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Nim</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nim}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Tempat Tanggal Lahir</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->tempat_lahir}}, {{$wisu->tanggal_lahir}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Alamat Asal</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->alamat_domisili}}</td>
        </tr>

        <tr >
            <th style="padding-left: 15px;">Nama Orangtua</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nama_ortu}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">No. Telp</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nohp}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Email</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->email}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Program Studi</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->prodi}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">IPK</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->ipk_mahasiswa}}</td>
        </tr>

        <tr >
          <td colspan="4" class="garis_bawah"> </td>
        </tr>
        <tr >
          <td colspan="4" style="padding-top: 10px;"> </td>
        </tr>
        @endif
        @endif
        @endif
        @endif
        @endforeach
        @endforeach
    </table>

  </center>

</main>
 
</body>
</html>