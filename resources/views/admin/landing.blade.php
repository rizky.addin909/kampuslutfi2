@extends('../layout/applog')

@section('title','Nama Website')


<!-- Page content -->
@section('content')
  <!-- Header -->
  <header class="w3-container" style="padding-top:22px">
      @foreach( $users as $user)
      @if($user->id == auth()->user()->id)
      @if($user->status =='admin')
      <h4><span>Dashboard <strong>{{ Auth::user()->status }}</strong></span><br><h4>
      @endif
      @if($user->status =='adminsuper')
      <h4><span>Dashboard <strong>{{ Auth::user()->status }}</strong></span><br><h4>
      @endif
      @if($user->status =='Mahasiswa')
      <h4><span>Dashboard <strong>{{ Auth::user()->status }}</strong></span><br><h4>
      <!--<a href="{{ route('siswaedit') }}" class="w3-bar-item w3-button"><i class="fa fa-user"></i></a>-->
      @endif
      @endif
      @endforeach
  </header>
  @foreach( $users as $user)
  @if($user->id == auth()->user()->id)
  @if($user->status =='admin')
  <div class="w3-row-padding w3-margin-bottom">
    <div class="w3-quarter">
      <div class="w3-container w3-red w3-padding-16">
        <div class="w3-left"><i class="fa fa-comment w3-xxxlarge"></i></div>
        <div class="w3-right">
          @foreach($wisudas as $wisud)
          <h3>{{$wisud->count()}}</h3>
          @endforeach
        </div>
        <div class="w3-clear"></div>
        <h4>Status Siswa</h4>
      </div>
    </div>
    <div class="w3-quarter">
      <div class="w3-container w3-orange w3-text-white w3-padding-16">
        <div class="w3-left"><i class="fa fa-users w3-xxxlarge"></i></div>
        <div class="w3-right">
          @foreach($jumser as $jum)
          <h3>{{$jum->where('status', 'siswa')->count()}}</h3>
          @endforeach
        </div>
        <div class="w3-clear"></div>
        <h4>Users</h4>
      </div>
    </div>
  </div>
  @endif
  @if($user->status =='adminsuper')
  <div class="w3-row-padding w3-margin-bottom">
    <div class="w3-quarter">
      <div class="w3-container w3-red w3-padding-16">
        <div class="w3-left"><i class="fa fa-comment w3-xxxlarge"></i></div>
        <div class="w3-right">
          @foreach($wisudas as $wisud)
          <h3>{{$wisud->count()}}</h3>
          @endforeach
        </div>
        <div class="w3-clear"></div>
        <h4>Status Siswa</h4>
      </div>
    </div>
    <div class="w3-quarter">
      <div class="w3-container w3-orange w3-text-white w3-padding-16">
        <div class="w3-left"><i class="fa fa-users w3-xxxlarge"></i></div>
        <div class="w3-right">
          @foreach($jumser as $jum)
          <h3>{{$jum->where('status', 'siswa')->count()}}</h3>
          @endforeach
        </div>
        <div class="w3-clear"></div>
        <h4>Users</h4>
      </div>
    </div>
  </div>
  @endif
  @if($user->status =='Mahasiswa')
  <div class="w3-row-padding w3-margin-bottom">
    <div class="w3-quarter">
      @foreach($siswas as $siswa)
      @if($siswa->user_id == auth()->user()->id)
      @if($siswa->status == 'Lulus')
        <div class="w3-container w3-green w3-padding-16">
          <div class="w3-left"><i class="fa fa-check-square-o w3-xxxlarge"></i></div>
            <div class="w3-right">
              <h3>Lulus</h3>
            </div>
            <div class="w3-clear"></div>
          <h4>Status</h4>
        </div>
        @else
        <div class="w3-container w3-red w3-padding-16">
          <div class="w3-left"><i class="fa fa-times w3-xxxlarge"></i></div>
            <div class="w3-right">
              <h3>Proses</h3>
            </div>
            <div class="w3-clear"></div>
            <h4>Status</h4>
        </div>
      @endif
      @endif
      @endforeach
    </div>
  </div>
  @endif
  @endif
  @endforeach
  
  <br>
<!--  <div class="w3-container w3-dark-grey w3-padding-32">
    <div class="w3-row">
      <div class="w3-container w3-third">
        <h5 class="w3-bottombar w3-border-green">Demographic</h5>
        <p>Language</p>
        <p>Country</p>
        <p>City</p>
      </div>
      <div class="w3-container w3-third">
        <h5 class="w3-bottombar w3-border-red">System</h5>
        <p>Browser</p>
        <p>OS</p>
        <p>More</p>
      </div>
      <div class="w3-container w3-third">
        <h5 class="w3-bottombar w3-border-orange">Target</h5>
        <p>Users</p>
        <p>Active</p>
        <p>Geo</p>
        <p>Interests</p>
      </div>
    </div>
  </div>
-->
  @endsection

  @section('footer')
  <!-- Footer -->
  <footer class="w3-container w3-padding-16 w3-light-grey">
    <center><p>Copyright © 1994 - 2020 LPP YOGYAKARTA. </p></center>
  </footer>
@endsection
