@extends('../layout/applog')

@section('title','Nama Website')


<!-- Page content -->
@section('content')
  <!-- Header -->
  <header class="w3-container" style="padding-top:22px">
    <h4><span>Data <strong>Yudisium</strong></span><br><h4>
  </header>

  <div class="w3-panel">
    <div class="w3-row-padding" style="margin:0 -16px">
      <div class="w3-twothird">
        <h5>Feeds</h5>
        <table class="w3-table w3-striped w3-white">
          @foreach( $wisuda as $wisud)
          @if($wisud->id_siswa == $id)
              <form action="{{route('editwisudaupdate')}}" method="post">
                @csrf
                <tr>
                  <td>
                    <p>Email : </p>
                    {{ $wisud->email }}
                  </td>
                </tr>

                <tr>
                  <td>
                    <p>Nama Lengkap : </p>
                    {{ $wisud->nama_lengkap }}
                  </td>
                </tr>

                <tr>
                  <td>
                    <p>Nim : </p>
                    {{ $wisud->nim }}
                  </td>
                </tr>

                <tr>
                  <td>
                    <p>Alamat Email : </p>
                    {{ $wisud->alamat_email }}
                  </td>
                </tr>

                <td>
                    <p>No Handphone (Masih Aktif) : </p>
                    {{ $wisud->nohp }}
                  </td>
                </tr>

                <td>
                    <p>Nama Orang Tua : </p>
                    {{ $wisud->nama_ortu }}
                  </td>
                </tr>

                <td>
                    <p>Program Studi : </p>
                    {{ $wisud->prodi }}
                  </td>
                </tr>

                <td>
                    <p>Tempat Lahir (Sesuai Kartu Keluarga) : </p>
                    {{ $wisud->tempat_lahir }}
                  </td>
                </tr>

                <td>
                    <p>Tanggal Lahir : </p>
                    {{ $wisud->tanggal_lahir }}
                  </td>
                </tr>

                <tr>
                  <td>
                    <p>Alamat Domisili (Asal) : </p>
                    {{ $wisud->alamat_domisili }}
                  </td>
                </tr>

                <tr>
                <td>
                    <p>NIK (Nomor Induk Kependudukan) : </p>
                    {{ $wisud->nik }}
                  </td>
                </tr>
                
                <tr>
                <td>
                    <p>Tempat PKL I </p>
                    {{ $wisud->tempat_pkl1 }}
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Tanggal Pelaksanaan I </p>
                    {{ $wisud->tanggal_pelaksanaan }}
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Judul PKL I </p>
                    {{ $wisud->judul_pkl1 }}
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Tempat PKL II </p>
                    {{ $wisud->tempat_pkl2 }}
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Tanggal Pelaksanaan II </p>
                    {{ $wisud->tanggal_pelaksanaan2 }}
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Judul PKL II </p>
                    {{ $wisud->judul_pkl2 }}
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Tempat PKL III </p>
                    {{ $wisud->tempat_pkl3 }}
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Tanggal Pelaksanaan III </p>
                    {{ $wisud->tanggal_pelaksanaan3 }}
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Judul PKL III </p>
                    {{ $wisud->judul_pkl3 }}
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Judul Tugas Akhir </p>
                    {{ $wisud->judul_tugas_akhir }}
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Nama Pembimbing Tugas Akhir (1) </p>
                    {{ $wisud->nama_pembimbing_tugas_akhir1 }}
                  </td>
                </tr>
                <tr>
                <td>
                    <p>Nama Pembimbing Tugas Akhir (2) </p>
                    {{ $wisud->nama_pembimbing_tugas_akhir2 }}
                  </td>
                </tr>
                <tr>
                <td>
                    <p>Nama Pembimbing Tugas Akhir (3) </p>
                    {{ $wisud->nama_pembimbing_tugas_akhir3 }}
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Nama Penguji Tugas Akhir (1) </p>
                    {{ $wisud->nama_penguji_tugas_akhir1 }}
                  </td>
                </tr>
                <tr>
                <td>
                    <p>Nama Penguji Tugas Akhir (2) </p>
                    {{ $wisud->nama_penguji_tugas_akhir2 }}
                  </td>
                </tr>
                <tr>
                <td>
                    <p>Nama Penguji Tugas Akhir (3) </p>
                    {{ $wisud->nama_penguji_tugas_akhir3 }}
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Ipk Mahasiswa </p>
                    {{ $wisud->ipk_mahasiswa }}
                  </td>
                </tr>
                  <input class="uk-input uk-width-1-1 uk-margin-large-left" type="hidden" name="sisid" value="{{ $wisud->id_siswa }}">

                <tr>
                  <td>
                    <p>Upload Scan Asli Kartu Keluaga (file PDF) : </p>
                   <p> <iframe src=" {{url('/uploads/wisudas/'.$wisud->nama_lengkap.'/'.$wisud->file_kk)}}" height="500px" width="500px"></iframe></p> 
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Upload Scan Asli Akte Kelahiran (File PDF) : </p>
                    <p> <iframe src=" {{url('uploads/wisudas/'.$wisud->nama_lengkap.'/'.$wisud->file_akte)}}" height="500px" width="500px"></iframe></p> 
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Upload Foto Berwarna Background Putih</p>
                   <p> <iframe src=" {{url('uploads/wisudas/'.$wisud->nama_lengkap.'/'.$wisud->file_bbp)}}" height="500px" width="500px"></iframe></p> 
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Upload Foto Berwarna Background Merah </p>
                    <p> <iframe src=" {{url('uploads/wisudas/'.$wisud->nama_lengkap.'/'.$wisud->file_bbm)}}" height="500px" width="500px"></iframe></p> 
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Upload Ijazah SMA/SMK/MA/Paket C</p>
                   <p> <iframe src=" {{url('uploads/wisudas/'.$wisud->nama_lengkap.'/'.$wisud->file_ijazah)}}" height="500px" width="500px"></iframe></p> 
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Upload Scan KTP Asli </p>
                    <p> <iframe src=" {{url('uploads/wisudas/'.$wisud->nama_lengkap.'/'.$wisud->file_ktp)}}" height="500px" width="500px"></iframe></p> 
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Upload Scan Bukti Pembayaran Biaya Wisuda</p>
                   <p> <iframe src=" {{url('uploads/wisudas/'.$wisud->nama_lengkap.'/'.$wisud->file_bpbw)}}" height="500px" width="500px"></iframe></p> 
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Upload Scan Bukti Penyerahan KTM </p>
                    <p> <iframe src=" {{url('uploads/wisudas/'.$wisud->nama_lengkap.'/'.$wisud->file_bpktm)}}" height="500px" width="500px"></iframe></p> 
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Upload Scan Lembar Pengesahan Tugas Akhir</p>
                   <p> <iframe src=" {{url('uploads/wisudas/'.$wisud->nama_lengkap.'/'.$wisud->file_lpta)}}" height="500px" width="500px"></iframe></p> 
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Upload Scan Sertifikat PPK (Program Pengenalan Kampus) </p>
                    <p> <iframe src=" {{url('uploads/wisudas/'.$wisud->nama_lengkap.'/'.$wisud->file_sppk)}}" height="500px" width="500px"></iframe></p> 
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Upload Scan Bukti Penyerahan Laporan PKL I, II, III dan Proyek Akhir</p>
                   <p> <iframe src=" {{url('uploads/wisudas/'.$wisud->nama_lengkap.'/'.$wisud->file_bplpkl)}}" height="500px" width="500px"></iframe></p> 
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Upload Scan Bukti Bebas Tanggungan Pustaka (Politeknik LPP) </p>
                    <p> <iframe src=" {{url('uploads/wisudas/'.$wisud->nama_lengkap.'/'.$wisud->file_bbtp)}}" height="500px" width="500px"></iframe></p> 
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Upload Scan Bukti Bebas Tanggungan Administrasi (Bagian Keuangan)</p>
                   <p> <iframe src=" {{url('uploads/wisudas/'.$wisud->nama_lengkap.'/'.$wisud->file_bbta)}}" height="500px" width="500px"></iframe></p> 
                  </td>
                </tr>

                <tr>
                <td>
                    <p>Upload Scan Surat Keterangan mengikuti Kegiatan Wajib Non-Akademik Bela Diri </p>
                    <p> <iframe src=" {{url('uploads/wisudas/'.$wisud->nama_lengkap.'/'.$wisud->file_beladiri)}}" height="500px" width="500px"></iframe></p> 
                  </td>
                </tr>
                @foreach( $siswas as $siswa)
                @if($siswa->id == $id)
                <tr>
                  <td>
                    <p>Status Mahasiswa : </p>
                    <select class="uk-input uk-width-1-4 uk-margin-large-left form-control" name="stat" id="stat">
                            <option value="{{$siswa->status}}">{{$siswa->status}}</option>
                            <option value="">Pilih Status</option>
                            <option value="Lulus">Lulus</option>
                            <option value="Proses">Proses</option>
                    </select>
                  </td>
                </tr>
                @endif
                @endforeach
                <tr>
                  <td>
                    <p>Komentar : </p>
                    <textarea class="uk-input uk-width-1-4 uk-margin-large-left form-control" name="komentar"> {{ $wisud->komentar }} </textarea>
                  </td>
                </tr>

                <tr>
                  <td>
                    <input type="submit" value="Kirim">
                  </td>
                </tr>

              </form>
              <tr>
                <td>
                  <form action="{{ route('editwisudacetak', $wisud->id_siswa) }}" method="get">
                    @csrf
                    <input id="iduser" type="hidden"  name="idsiswa" value="{{$wisud->id_siswa}}">
                    <input type="submit" value="CETAK PDF">
                  </form>
                </td>
              </tr>
          @endif
          @endforeach
        </table>
      </div>
    </div>
  </div>
  @endsection

  @section('footer')
  <!-- Footer -->
  <footer class="w3-container w3-padding-16 w3-light-grey">
    <center><p>Copyright © 1994 - 2020 LPP YOGYAKARTA. </p></center>
  </footer>
@endsection
