@extends('../layout/applog')

  
<style>
        table {
          border-collapse: collapse;
          border: 0px solid black;
        }
        th,td {
            font-size: 15px;
        }
          .garis_atas{
            border-bottom: 3px black solid;
            height: 5px;
            width: 0px;
          }
          .garis_bawah{
            border-bottom: 3px black solid;
            height: 5px;
            width: 0px;
          }
</style>
@section('title','Nama Website')


<!-- Page content -->
@section('content')
  <!-- Header -->
  <header class="w3-container" style="padding-top:22px">
    <h4><span>Data <strong>Yudisium</strong></span><br><h4>
  </header>

  <div class="container">
    <br/>
    <a href="{{ route('cetakbukualumni') }}" class="btn btn-primary" target="_blank">CETAK PDF</a>
    <br>
      

    <table align="center">
      <tr>
        <td colspan="4" style="font-size: 30px; text-align: center;"><p><b>KATA PENGANTAR</b></p></td>
      </tr>

      <tr>
        <td colspan="4" style="font-size: 17px; text-align: justify; text-indent: 60px;"><p>Assalamualaikum wr.wb.</p></td>
      </tr>
      <tr>
        <td colspan="4" style="font-size: 17px; text-align: justify; text-indent: 60px;"><p>Dengan mengucapkan puji dan syukur kehadirat Allah Yang Maha Pengasih dan Penyayang, Politeknik LPP diberi kesempatan dan kemudahan untuk melepaskan lulusan dalam acara wisuda ke-19 tahun 2019. Seluruh jajaran pengelola dan pelaksana mengucapkan <br>selamat dan sukses bagi seluruh wisudawan yangtelah menyelesaikan studi di Politeknik LPP.</p></td>
      </tr>
      <tr>
        <td colspan="4" style="font-size: 17px; text-align: justify; text-indent: 60px;"><p>Selanjutnya kami mengucapkan selamat berkarya dan men-gamalkan ilmu untuk kepentingan bangsa dan negara kita tercinta. Jadilah pribadi-pribadi yang jujur, ikhlas, mandiri, dan berkarakter ditengah masyarakat.</p></td>
      </tr>
      <tr>
        <td colspan="4" style="font-size: 17px; text-align: justify; text-indent: 60px;"><p>Do’a kami menyertai seluruh wisudawan/wati.</p></td>
      </tr>
      <tr>
        <td colspan="4" style="font-size: 17px; text-align: justify; text-indent: 60px;"><p>Wassalamu’alaikum wr.wb.</p></td>
      </tr>
    </table>

    <table align="center" style=" padding-top: 100%; ">
      <tr>
        <td colspan="4" style="font-size: 30px; text-align: center;"><p><b>DAFTAR ISI</b></p></td>
      </tr>

      <tr>
        <td colspan="4" style="font-size: 17px; text-align: justify; text-indent: 60px;"><p><b>PENGANTAR</b>......................................................................................................... i</p></td>
      </tr>
      <tr>
        <td colspan="4" style="font-size: 17px; text-align: justify; text-indent: 60px;"><p><b>DAFTAR ISI</b>........................................................................................................... ii</p></td>
      </tr>
      <tr>
        <td colspan="4" style="font-size: 17px; text-align: justify; text-indent: 60px;"><p><b>Lulusan Terbaik</b>................................................................................................... 1</p></td>
      </tr>
      <tr>
        <td colspan="4" style="font-size: 17px; text-align: justify; text-indent: 60px;"><p><b>D3 Teknik Kimia</b>................................................................................................... 1</p></td>
      </tr>
      <tr>
        <td colspan="4" style="font-size: 17px; text-align: justify; text-indent: 60px;"><p><b>D3 Teknik Kimia</b>................................................................................................. 13</p></td>
      </tr>
      <tr>
        <td colspan="4" style="font-size: 17px; text-align: justify; text-indent: 60px;"><p><b>D3 Akuntansi</b>...................................................................................................... 18</p></td>
      </tr>
      <tr>
        <td colspan="4" style="font-size: 17px; text-align: justify; text-indent: 60px;"><p><b>D3 Budidaya Tanaman Perkebunan</b>................................................................. 21</p></td>
      </tr>
      <tr>
        <td colspan="4" style="font-size: 17px; text-align: justify; text-indent: 60px;"><p><b>D4 Budidaya Tanaman Perkebunan</b>................................................................. 35</p></td>
      </tr>
    </table>

    <center>
    
    <table align="center" style=" padding-top: 100%; ">
      <tr>
        <td colspan="4" style="font-size: 30px; text-align: center;"><p><b>LULUSAN TERBAIK</b></p></td>
      </tr>
        <tr >
          <td colspan="4" class="garis_atas"> </td>
        </tr>
        <tr >
          <td colspan="4" style="padding-top: 10px;"> </td>
        </tr>
      @foreach($wisuda as $wisu)
      @foreach($siswas as $sis)
      @if($sis->status == 'Lulus')
      @if($sis->id == $wisu->id_siswa)
      @if($wisu->ipk_mahasiswa > '3.50')
        
        <tr>
            <th rowspan="10" style=""><center><img src="uploads/wisudas/{{$wisu->nama_lengkap}}/{{$wisu->file_bbm}}" style="margin-left: 15px; margin-right: 10px;  border: 3px solid black ; " width="140px" height="180"></th>
        </tr>
        <tr>
            <th style="padding-left: 15px;">Nama </th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nama_lengkap}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Nim</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nim}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Tempat Tanggal Lahir</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->tempat_lahir}}, {{$wisu->tanggal_lahir}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Alamat Asal</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->alamat_domisili}}</td>
        </tr>

        <tr >
            <th style="padding-left: 15px;">Nama Orangtua</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nama_ortu}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">No. Telp</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nohp}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Email</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->email}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Program Studi</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->prodi}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">IPK</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->ipk_mahasiswa}}</td>
        </tr>

        <tr >
          <td colspan="4" class="garis_bawah"> </td>
        </tr>
        <tr >
          <td colspan="4" style="padding-top: 10px;"> </td>
        </tr>
        @endif
        @endif
        @endif
        @endforeach
        @endforeach
    </table>
  </center>

  <center>
    <table align="center" style=" padding-top: 100%; ">
      <tr>
        <td colspan="4" style="font-size: 30px; text-align: center;"><p><b>D3 TEKNIK KIMIA</b></p></td>
      </tr>
        <tr >
          <td colspan="4" class="garis_atas"> </td>
        </tr>
        <tr >
          <td colspan="4" style="padding-top: 10px;"> </td>
        </tr>
      @foreach($wisuda as $wisu)
      @foreach($siswas as $sis)
      @if($sis->status == 'Lulus')
      @if($sis->id == $wisu->id_siswa)
      @if($wisu->prodi == 'Teknik Kimia')
      @if($wisu->ipk_mahasiswa <= '3.50')
        
        <tr>
            <th rowspan="10" style=""><center><img src="uploads/wisudas/{{$wisu->nama_lengkap}}/{{$wisu->file_bbm}}" style="margin-left: 15px; margin-right: 10px;  border: 3px solid black ; " width="140px" height="180"></th>
        </tr>
        <tr>
            <th style="padding-left: 15px;">Nama </th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nama_lengkap}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Nim</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nim}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Tempat Tanggal Lahir</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->tempat_lahir}}, {{$wisu->tanggal_lahir}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Alamat Asal</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->alamat_domisili}}</td>
        </tr>

        <tr >
            <th style="padding-left: 15px;">Nama Orangtua</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nama_ortu}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">No. Telp</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nohp}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Email</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->email}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Program Studi</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->prodi}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">IPK</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->ipk_mahasiswa}}</td>
        </tr>

        <tr >
          <td colspan="4" class="garis_bawah"> </td>
        </tr>
        <tr >
          <td colspan="4" style="padding-top: 10px;"> </td>
        </tr>
        @endif
        @endif
        @endif
        @endif
        @endforeach
        @endforeach
    </table>

  </center>

  <center>
    <table align="center" style=" padding-top: 100%; ">
      <tr>
        <td colspan="4" style="font-size: 30px; text-align: center;"><p><b>D3 TEKNIK MESIN</b></p></td>
      </tr>
        <tr >
          <td colspan="4" class="garis_atas"> </td>
        </tr>
        <tr >
          <td colspan="4" style="padding-top: 10px;"> </td>
        </tr>
      @foreach($wisuda as $wisu)
      @foreach($siswas as $sis)
      @if($sis->status == 'Lulus')
      @if($sis->id == $wisu->id_siswa)
      @if($wisu->prodi == 'Teknik Mesin')
      @if($wisu->ipk_mahasiswa <= '3.50')
        
        <tr>
            <th rowspan="10" style=""><center><img src="uploads/wisudas/{{$wisu->nama_lengkap}}/{{$wisu->file_bbm}}" style="margin-left: 15px; margin-right: 10px;  border: 3px solid black ; " width="140px" height="180"></th>
        </tr>
        <tr>
            <th style="padding-left: 15px;">Nama </th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nama_lengkap}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Nim</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nim}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Tempat Tanggal Lahir</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->tempat_lahir}}, {{$wisu->tanggal_lahir}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Alamat Asal</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->alamat_domisili}}</td>
        </tr>

        <tr >
            <th style="padding-left: 15px;">Nama Orangtua</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nama_ortu}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">No. Telp</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nohp}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Email</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->email}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Program Studi</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->prodi}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">IPK</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->ipk_mahasiswa}}</td>
        </tr>

        <tr >
          <td colspan="4" class="garis_bawah"> </td>
        </tr>
        <tr >
          <td colspan="4" style="padding-top: 10px;"> </td>
        </tr>
        @endif
        @endif
        @endif
        @endif
        @endforeach
        @endforeach
    </table>

  </center>

  <center>
    <table align="center" style=" padding-top: 100%; ">
      <tr>
        <td colspan="4" style="font-size: 30px; text-align: center;"><p><b>D3 AKUNTANSI</b></p></td>
      </tr>
        <tr >
          <td colspan="4" class="garis_atas"> </td>
        </tr>
        <tr >
          <td colspan="4" style="padding-top: 10px;"> </td>
        </tr>
      @foreach($wisuda as $wisu)
      @foreach($siswas as $sis)
      @if($sis->status == 'Lulus')
      @if($sis->id == $wisu->id_siswa)
      @if($wisu->prodi == 'Akuntansi')
      @if($wisu->ipk_mahasiswa <= '3.50')
        
        <tr>
            <th rowspan="10" style=""><center><img src="uploads/wisudas/{{$wisu->nama_lengkap}}/{{$wisu->file_bbm}}" style="margin-left: 15px; margin-right: 10px;  border: 3px solid black ; " width="140px" height="180"></th>
        </tr>
        <tr>
            <th style="padding-left: 15px;">Nama </th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nama_lengkap}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Nim</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nim}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Tempat Tanggal Lahir</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->tempat_lahir}}, {{$wisu->tanggal_lahir}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Alamat Asal</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->alamat_domisili}}</td>
        </tr>

        <tr >
            <th style="padding-left: 15px;">Nama Orangtua</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nama_ortu}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">No. Telp</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nohp}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Email</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->email}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Program Studi</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->prodi}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">IPK</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->ipk_mahasiswa}}</td>
        </tr>

        <tr >
          <td colspan="4" class="garis_bawah"> </td>
        </tr>
        <tr >
          <td colspan="4" style="padding-top: 10px;"> </td>
        </tr>
        @endif
        @endif
        @endif
        @endif
        @endforeach
        @endforeach
    </table>

  </center>

  <center>
    <table align="center" style=" padding-top: 100%; ">
      <tr>
        <td colspan="4" style="font-size: 30px; text-align: center;"><p><b>D3 BUDIDAYA TANAMAN <p><b>PERKEBUNAN</b></p></b></p></td>
      </tr>
        <tr >
          <td colspan="4" class="garis_atas"> </td>
        </tr>
        <tr >
          <td colspan="4" style="padding-top: 10px;"> </td>
        </tr>
      @foreach($wisuda as $wisu)
      @foreach($siswas as $sis)
      @if($sis->status == 'Lulus')
      @if($sis->id == $wisu->id_siswa)
      @if($wisu->prodi == 'Budidaya Tanaman Perkebunan D3')
      @if($wisu->ipk_mahasiswa <= '3.50')
        
        <tr>
            <th rowspan="10" style=""><center><img src="uploads/wisudas/{{$wisu->nama_lengkap}}/{{$wisu->file_bbm}}" style="margin-left: 15px; margin-right: 10px;  border: 3px solid black ; " width="140px" height="180"></th>
        </tr>
        <tr>
            <th style="padding-left: 15px;">Nama </th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nama_lengkap}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Nim</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nim}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Tempat Tanggal Lahir</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->tempat_lahir}}, {{$wisu->tanggal_lahir}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Alamat Asal</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->alamat_domisili}}</td>
        </tr>

        <tr >
            <th style="padding-left: 15px;">Nama Orangtua</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nama_ortu}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">No. Telp</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nohp}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Email</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->email}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Program Studi</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->prodi}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">IPK</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->ipk_mahasiswa}}</td>
        </tr>

        <tr >
          <td colspan="4" class="garis_bawah"> </td>
        </tr>
        <tr >
          <td colspan="4" style="padding-top: 10px;"> </td>
        </tr>
        @endif
        @endif
        @endif
        @endif
        @endforeach
        @endforeach
    </table>

  </center>

  <center>
    <table align="center" style=" padding-top: 100%; ">
      <tr>
        <td colspan="4" style="font-size: 30px; text-align: center;"><p><b>D4 BUDIDAYA TANAMAN <p><b>PERKEBUNAN</b></p></b></p></td>
      </tr>
        <tr >
          <td colspan="4" class="garis_atas"> </td>
        </tr>
        <tr >
          <td colspan="4" style="padding-top: 10px;"> </td>
        </tr>
      @foreach($wisuda as $wisu)
      @foreach($siswas as $sis)
      @if($sis->status == 'Lulus')
      @if($sis->id == $wisu->id_siswa)
      @if($wisu->prodi == 'Budidaya Tanaman Perkebunan D4')
      @if($wisu->ipk_mahasiswa <= '3.50')
        
        <tr>
            <th rowspan="10" style=""><center><img src="uploads/wisudas/{{$wisu->nama_lengkap}}/{{$wisu->file_bbm}}" style="margin-left: 15px; margin-right: 10px;  border: 3px solid black ; " width="140px" height="180"></th>
        </tr>
        <tr>
            <th style="padding-left: 15px;">Nama </th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nama_lengkap}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Nim</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nim}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Tempat Tanggal Lahir</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->tempat_lahir}}, {{$wisu->tanggal_lahir}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Alamat Asal</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->alamat_domisili}}</td>
        </tr>

        <tr >
            <th style="padding-left: 15px;">Nama Orangtua</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nama_ortu}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">No. Telp</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->nohp}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Email</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->email}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">Program Studi</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->prodi}}</td>
        </tr>
        <tr >
            <th style="padding-left: 15px;">IPK</th>
            <td style="padding-left: 50px;"> : </td>
            <td style="padding-left: 5px;"> {{$wisu->ipk_mahasiswa}}</td>
        </tr>

        <tr >
          <td colspan="4" class="garis_bawah"> </td>
        </tr>
        <tr >
          <td colspan="4" style="padding-top: 10px;"> </td>
        </tr>
        @endif
        @endif
        @endif
        @endif
        @endforeach
        @endforeach
    </table>

  </center>
 
  </div>
  @endsection

  @section('footer')
  <!-- Footer -->
  <footer class="w3-container w3-padding-16 w3-light-grey">
    <center><p>Copyright © 1994 - 2020 LPP YOGYAKARTA. </p></center>
  </footer>
@endsection
