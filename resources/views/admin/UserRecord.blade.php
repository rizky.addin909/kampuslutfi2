@extends('../layout/applog')

@section('title','Nama Website')


<!-- Page content -->
@section('content')
  <!-- Header -->
  <header class="w3-container" style="padding-top:22px">
    <h4><span>Data <strong>User</strong></span><br><h4>
  </header>

  <div class="w3-panel">
    <div class="w3-row-padding" style="margin:0 -16px">
      <div class="w3-twothird">
        <h5>Feeds</h5>
        <table class="w3-table w3-striped w3-white">
          @foreach( $users as $user)
          <tr>
            <td>{{$user->name}}</i></td>
            <td>{{$user->email}}</td>
            <td><i>{{$user->status}}</i></td>
            @foreach( $siswas as $siswa)
            @if($user->id == $siswa->user_id)
            <td><i>{{$siswa->status}}</i></td>
            @endif
            @endforeach
            <td>
              <form action="{{ route('edituser', $user->id) }}" method="get">
                @csrf
                <input id="iduser" type="hidden"  name="iduser" value="{{$user->id}}">
                <input type="submit" value="edit">
              </form>
            </td>
          </tr>

          @endforeach
          
        </table>
      </div>
    </div>
  </div>
  @endsection

  @section('footer')
  <!-- Footer -->
  <footer class="w3-container w3-padding-16 w3-light-grey">
    <center><p>Copyright © 1994 - 2020 LPP YOGYAKARTA. </p></center>
  </footer>
@endsection
