-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 27, 2020 at 03:08 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kampus`
--

-- --------------------------------------------------------

--
-- Table structure for table `beritas`
--

CREATE TABLE `beritas` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `berita` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `beritas`
--

INSERT INTO `beritas` (`id`, `user_id`, `berita`, `created_at`, `updated_at`) VALUES
(6, 11, 'balok ini kempes', '2020-08-08 19:37:57', '2020-08-25 22:27:53'),
(8, 11, 'ini berita apaya', '2020-08-09 07:09:26', '2020-08-09 07:09:26');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('muhammad.1050@students.amikom.ac.id', '$2y$10$gHxO3Ra0Y1v4JoDq291SxebfGipEpiHmx/Yd4JML0dbX8CIoTkm8O', '2020-08-26 13:54:48'),
('mio.akiyama909@gmail.com', '$2y$10$02jSGYLSoWsmycn0neqX4u65.02fD0fb6rcXsn/blqrBJ7doJ4t86', '2020-08-26 17:58:31');

-- --------------------------------------------------------

--
-- Table structure for table `siswas`
--

CREATE TABLE `siswas` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `nama_depan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_belakang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telepon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `siswas`
--

INSERT INTO `siswas` (`id`, `user_id`, `nama_depan`, `nama_belakang`, `email`, `password`, `telepon`, `alamat`, `status`, `foto`, `created_at`, `updated_at`) VALUES
(9, 13, 'riztunah', 'local', 'admin@ayosinau.com', NULL, '087711870215', 'jl.sukun', 'Lulus', NULL, '2020-07-17 11:45:20', '2020-08-25 17:42:39'),
(10, 14, 'kotobuki', NULL, 'amikom@gmail.com', NULL, NULL, NULL, NULL, NULL, '2020-07-18 08:01:54', '2020-07-18 08:01:54'),
(12, 15, 'akashi record', NULL, 'lord@amikom.ac.id', NULL, NULL, NULL, 'Lulus', NULL, '2020-08-10 03:22:30', '2020-08-14 23:35:45'),
(13, 17, 'makino', NULL, 'makino@gmail.com', NULL, NULL, NULL, 'Lulus', NULL, '2020-08-21 10:35:00', '2020-08-21 10:35:00'),
(14, 18, 'addin saputra', NULL, 'addin.saput@gmail.com', NULL, NULL, NULL, 'Lulus', NULL, '2020-08-25 12:22:08', '2020-08-25 12:49:25');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nim` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(244) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `nim`, `avatar`, `email_verified_at`, `password`, `provider`, `provider_id`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(11, 'Rizky Addin', 'muhammad.1050@students.amikom.ac.id', NULL, '1595084677.png', NULL, '$2y$10$Yqwtef9GyOMWsBGBQi3TA.CBBb795wuMGDdnKJt3WJt.uVorsyMNK', NULL, NULL, 'admin', '1cdVVRsIOUw6HqGZpuaPdHS7dkXqt0rVLeKjIrZgX5TxTgm169JYsn2EZKDa', '2020-07-15 16:52:41', '2020-08-26 13:43:42'),
(12, 'akihito Hibari diana', 'mio.akiyama909@gmail.com', NULL, '1595084677.png', NULL, '$2y$10$2S3/yoD2w2xkvJj3zvg4P.Fmsi3AejYmh.hzbfJZwXaXPx5wc5Ykm', NULL, NULL, 'Mahasiswa', 'Qh7IlaBzaaTxIxBZvwOS84ooXGxmSwS4zvFMLdUaTBKM8jYBsvJw5fqJUDcY', '2020-07-16 23:10:44', '2020-08-26 17:24:05'),
(13, 'riztunah', 'admin@ayosinau.com', NULL, '1595054538.png', NULL, '$2y$10$shkM3YsYjITMBhAQesmwS.Hxw9/zeQZw.m82wfNjQ9NdpkIDO30xK', NULL, NULL, 'Mahasiswa', NULL, '2020-07-17 08:42:46', '2020-07-18 07:57:46'),
(14, 'kotobuki', 'amikom@gmail.com', NULL, '1595084677.png', NULL, '$2y$10$FMHZT/3jJ4lBAx2G7v7w/OW0ZkJYD5qgtCTW.8e7ww1qJVUi6HZu.', NULL, NULL, 'Mahasiswa', NULL, '2020-07-18 08:01:47', '2020-07-18 08:04:37'),
(15, 'akashi record', 'lord@amikom.ac.id', NULL, '1597057511.png', NULL, '$2y$10$F92I8o.8z./ugdOg/8hnEOfwDtrkT.9l/YSjznQy90x3Mx53ZNdRW', NULL, NULL, 'Mahasiswa', NULL, '2020-08-10 03:22:27', '2020-08-14 23:35:45'),
(16, 'admin super', 'adminsuper@gmail.com', NULL, '1595084677.png', NULL, '$2y$10$8xDU.mKNR0kUzIjvjykNT.ElmyeJeSxn5yJ5epopq/40FCepaOU4q', NULL, NULL, 'adminsuper', NULL, '2020-08-14 23:12:00', '2020-08-14 23:12:00'),
(17, 'makino', 'makino@gmail.com', '17.11.1050', '1598041496.png', NULL, '$2y$10$sVEuRIkyKxGd.ODEi2VFeei/UWW9nENBbpgXT6qOrhHYtcEvFGXny', NULL, NULL, 'Mahasiswa', NULL, '2020-08-21 10:34:55', '2020-08-21 13:24:56'),
(18, 'addin saputra', 'addin.saput@gmail.com', 'addin.saput@gmail.com', '1598383340.png', NULL, '$2y$10$yfw1uZtDcghu6EzS1ofyjeeE705NGBDjFe7zLTJWmg6HlpXwfAeZG', NULL, NULL, 'Mahasiswa', NULL, '2020-08-25 12:22:05', '2020-08-25 12:22:20');

-- --------------------------------------------------------

--
-- Table structure for table `wisudas`
--

CREATE TABLE `wisudas` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_siswa` int(10) UNSIGNED DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `nama_lengkap` varchar(255) DEFAULT NULL,
  `nim` varchar(255) DEFAULT NULL,
  `alamat_email` varchar(255) DEFAULT NULL,
  `nohp` varchar(255) DEFAULT NULL,
  `nama_ortu` varchar(255) DEFAULT NULL,
  `prodi` varchar(255) DEFAULT NULL,
  `tempat_lahir` varchar(255) DEFAULT NULL,
  `tanggal_lahir` varchar(255) DEFAULT NULL,
  `alamat_domisili` varchar(255) DEFAULT NULL,
  `nik` varchar(255) DEFAULT NULL,
  `file_kk` varchar(255) DEFAULT NULL,
  `file_akte` varchar(255) DEFAULT NULL,
  `tempat_pkl1` varchar(255) DEFAULT NULL,
  `tanggal_pelaksanaan` varchar(255) DEFAULT NULL,
  `judul_pkl1` varchar(255) DEFAULT NULL,
  `tempat_pkl2` varchar(255) DEFAULT NULL,
  `tanggal_pelaksanaan2` varchar(255) DEFAULT NULL,
  `judul_pkl2` varchar(255) DEFAULT NULL,
  `tempat_pkl3` varchar(255) DEFAULT NULL,
  `tanggal_pelaksanaan3` varchar(255) DEFAULT NULL,
  `judul_pkl3` varchar(255) DEFAULT NULL,
  `judul_tugas_akhir` varchar(255) DEFAULT NULL,
  `nama_pembimbing_tugas_akhir1` varchar(255) DEFAULT NULL,
  `nama_pembimbing_tugas_akhir2` varchar(255) DEFAULT NULL,
  `nama_pembimbing_tugas_akhir3` varchar(255) DEFAULT NULL,
  `nama_penguji_tugas_akhir1` varchar(255) DEFAULT NULL,
  `nama_penguji_tugas_akhir2` varchar(255) DEFAULT NULL,
  `nama_penguji_tugas_akhir3` varchar(255) DEFAULT NULL,
  `ipk_mahasiswa` decimal(3,2) DEFAULT NULL,
  `file_bbp` varchar(255) DEFAULT NULL,
  `file_bbm` varchar(255) DEFAULT NULL,
  `file_ijazah` varchar(255) DEFAULT NULL,
  `file_ktp` varchar(255) DEFAULT NULL,
  `file_bpbw` varchar(255) DEFAULT NULL,
  `file_bpktm` varchar(255) DEFAULT NULL,
  `file_lpta` varchar(255) DEFAULT NULL,
  `file_sppk` varchar(255) DEFAULT NULL,
  `file_bplpkl` varchar(255) DEFAULT NULL,
  `file_bbtp` varchar(255) DEFAULT NULL,
  `file_bbta` varchar(255) DEFAULT NULL,
  `file_beladiri` varchar(255) DEFAULT NULL,
  `komentar` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wisudas`
--

INSERT INTO `wisudas` (`id`, `id_siswa`, `email`, `nama_lengkap`, `nim`, `alamat_email`, `nohp`, `nama_ortu`, `prodi`, `tempat_lahir`, `tanggal_lahir`, `alamat_domisili`, `nik`, `file_kk`, `file_akte`, `tempat_pkl1`, `tanggal_pelaksanaan`, `judul_pkl1`, `tempat_pkl2`, `tanggal_pelaksanaan2`, `judul_pkl2`, `tempat_pkl3`, `tanggal_pelaksanaan3`, `judul_pkl3`, `judul_tugas_akhir`, `nama_pembimbing_tugas_akhir1`, `nama_pembimbing_tugas_akhir2`, `nama_pembimbing_tugas_akhir3`, `nama_penguji_tugas_akhir1`, `nama_penguji_tugas_akhir2`, `nama_penguji_tugas_akhir3`, `ipk_mahasiswa`, `file_bbp`, `file_bbm`, `file_ijazah`, `file_ktp`, `file_bpbw`, `file_bpktm`, `file_lpta`, `file_sppk`, `file_bplpkl`, `file_bbtp`, `file_bbta`, `file_beladiri`, `komentar`, `created_at`, `updated_at`) VALUES
(50, 9, 'admin@ayosinau.com', 'addin saputra', '17.11.1050', 'admin@ayosinau.com', '0888484848', 'sarinah', 'Akuntansi', 'banjarbaru', '25/08/2020', 'banjarbaru', '115455255154', 'Ketua Kelas.pdf', 'rizky_CV.pdf', 'yogyakarta', '07/12/2017', 'krpo', 'yogyakarta', '07/12/2019', 'krpo', NULL, NULL, NULL, 'kepo', 'makinah', 'makinah ari', NULL, 'subatjo', 'sarui', NULL, '3.50', 'Screenshot from 2020-08-25 15-57-23.png', 'Screenshot from 2020-08-25 15-57-42.png', 'tugas12.doc', 'tugas12.doc', 'tugas12.doc', 'tugas12.doc', 'tugas12.doc', 'tugas12.doc', 'tugas12.doc', 'tugas12.doc', 'tugas12.doc', 'tugas12.doc', NULL, '2020-08-09 22:54:53', '2020-08-25 17:42:39'),
(51, 12, 'lord@amikom.ac.id', 'asdasdasd', '17.11.1050', 'lord@amikom.ac.id', '087884555', NULL, NULL, NULL, NULL, NULL, NULL, 'rizky_CV.pdf', 'Proses Pengajuan Judul TA-Skripsi serta Layanan yang Terkait TA-Skripsi.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '3.80', 'Ketua Kelas.pdf', 'Screenshot from 2020-08-24 03-19-06.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'mau dikomentarin apa ya bingung akutuh', '2020-08-10 03:22:41', '2020-08-25 00:42:12'),
(52, 13, 'makino@gmail.com', 'makino simanjuntak', '17.11.1050', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '4.00', 'contoh diagram kerangka berfikir ptk.png', 'contoh diagram kerangka berfikir ptk.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-21 11:40:26', '2020-08-25 00:40:48'),
(53, 14, 'addin.saput@gmail.com', 'Rizky Saputra', '17.11.1048', 'addin.saput@gmail.com', NULL, NULL, 'Teknik Kimia', NULL, '07/08/2020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '3.51', NULL, 'Screenshot from 2020-08-25 15-57-23.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-25 12:22:23', '2020-08-25 12:49:25');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `beritas`
--
ALTER TABLE `beritas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `siswas`
--
ALTER TABLE `siswas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wisudas`
--
ALTER TABLE `wisudas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_siswa` (`id_siswa`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `beritas`
--
ALTER TABLE `beritas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `siswas`
--
ALTER TABLE `siswas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `wisudas`
--
ALTER TABLE `wisudas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `beritas`
--
ALTER TABLE `beritas`
  ADD CONSTRAINT `beritas_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `siswas`
--
ALTER TABLE `siswas`
  ADD CONSTRAINT `siswas_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `wisudas`
--
ALTER TABLE `wisudas`
  ADD CONSTRAINT `wisudas_ibfk_1` FOREIGN KEY (`id_siswa`) REFERENCES `siswas` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
