<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','PageController@index');

Route::get('/index2','PageController@index2')->name('index2');

Route::post('/login2', 'Auth\LoginController@postlogin')->name('login2');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/password/reset','PageController@index2')->name('password.request');
Route::get('/login','PageController@index')->name('login');

//route admin start
Route::get('/viewuser', 'PageController@userview')->name('viewuser');
Route::get('/edituser/{id}', 'PageController@edituser')->name('edituser');
Route::post('/edituser/update', 'PageController@update')->name('edituserupdate');

Route::get('/view wisuda', 'PageController@wisudaview')->name('viewwisuda');
Route::get('/edit wisuda/{id}', 'PageController@editwisuda')->name('editwisuda');
Route::post('/edit wisuda/update', 'PageController@updatewisuda')->name('editwisudaupdate');
Route::get('/edit wisuda/cetak/{id}', 'PageController@editwisudacetak')->name('editwisudacetak');

Route::get('/tambah berita', 'PageController@tambahberitaview')->name('tambahberita');
Route::post('/tambah berita/update', 'PageController@tambahberita')->name('tambahberitaproses');
Route::get('/view berita', 'PageController@beritaview')->name('viewberita');
Route::get('/edit berita/{id}', 'PageController@editberita')->name('editberita');
Route::post('/edit berita/update', 'PageController@updateberita')->name('editberitaupdate');
Route::post('/hapus berita/{id}', 'PageController@deleteberita')->name('hapusberita');

Route::get('/Buku Alumni', 'PageController@bukualumniview')->name('viewbukualumni');
Route::get('/Buku Alumni/cetak', 'PageController@cetakbukualumni')->name('cetakbukualumni');
//route admin end

//route adminsuper start
Route::get('/view user super', 'PageController@userviewsuper')->name('viewusersuper');
Route::get('/edit user super/{id}', 'PageController@editusersuper')->name('editusersuper');
Route::post('/edit user super/update', 'PageController@updatesuper')->name('edituserupdatesuper');

Route::get('/view wisuda super', 'PageController@wisudaviewsuper')->name('viewwisudasuper');
Route::get('/edit wisuda super/{id}', 'PageController@editwisudasuper')->name('editwisudasuper');
Route::post('/edit wisuda super/update', 'PageController@updatewisudasuper')->name('editwisudaupdatesuper');

Route::get('/tambah berita super', 'PageController@tambahberitaviewsuper')->name('tambahberitasuper');
Route::post('/tambah berita super/update', 'PageController@tambahberitasuper')->name('tambahberitaprosessuper');
Route::get('/view berita super', 'PageController@beritaviewsuper')->name('viewberitasuper');
Route::get('/edit berita super/{id}', 'PageController@editberitasuper')->name('editberitasuper');
Route::post('/edit berita super/update', 'PageController@updateberitasuper')->name('editberitaupdatesuper');
Route::post('/hapus berita super/{id}', 'PageController@deleteberitasuper')->name('hapusberitasuper');
//route adminduper end

Route::get('/siswa', 'SiswaController@index')->name('siswa');
Route::get('/siswa/daftar', 'SiswaController@daftarwisuda')->name('daftarwisuda');
Route::get('/siswa/store/{id}', 'SiswaController@storewisuda1')->name('storewisuda1');
Route::post('/siswa/daftar/proses', 'SiswaController@storewisuda')->name('prosesdaftarwisuda');
Route::post('/avatar', 'SiswaController@update_avatar')->name('avatarsa');
Route::get('/store', 'SiswaController@store')->name('siswaedit');
Route::post('/update siswa', 'SiswaController@update')->name('siswa.update');
