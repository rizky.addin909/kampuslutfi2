<?php

namespace App\Http\Controllers;

use App\Siswa;
use Illuminate\Http\Request;

use Image;
use Auth;
use App\User;
use App\Wisuda;

class SiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $siswa = Siswa::get();
        if( $user == null ){
            abort(403,'Anda tidak punya akses ke halaman ini silahkan login');
        }else{
            $users = User::where('id', auth()->user()->id)->get();
            $siswas = Siswa::where('user_id', auth()->user()->id)->get();
            return view('siswa.SiswaLanding', compact('users', 'user', 'siswa', 'siswas' ));
        }
    }

    public function daftarwisuda()
    {
        $user = Auth::user();
        $siswa = Siswa::get();
        $wisud = Wisuda::get();
        if( $user == null ){
            abort(403,'Anda tidak punya akses ke halaman ini silahkan login');
        }else{
            $users = User::where('id', auth()->user()->id)->get();
            $siswas = Siswa::where('user_id', auth()->user()->id)->get();
            $siswad = Siswa::where('user_id', auth()->user()->id)->get('id')->first();
            $wisuds = Wisuda::where('id_siswa', $siswad['id'])->get();
            return view('siswa.SiswaWisuda', compact('users', 'user', 'siswa', 'siswas', 'wisud', 'wisuds' ));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = auth()->user()->id;
        $siswa = Siswa::where('user_id', $user)->get('user_id')->first();
        
        if(  $siswa['user_id'] != $user ){
            $siswa = siswa::create([
                'user_id' => Auth::user()->id,
                'nama_depan' => Auth::user()->name,
                'email' => Auth::user()->email,
		'status' => 'Proses'
            ]);
            return redirect()->route('siswa');
        }else{
            return redirect()->route('siswa');
        }
    }

    public function storewisuda(Request $request)
    {
        
        $wisud = Wisuda::where('id_siswa', $request->sisid)->get('id_siswa')->first();
            if($request->hasFile('filekk')){
                $files1= $request->file('filekk');
                
                $ns=0;
                    $filename1 = $files1->getClientOriginalName();
                    $foldername1 = $request->namlep;
                    $NameArr1 = $filename1;
                    $files1->move('uploads/wisudas/' . $foldername1 , $filename1);

                    Wisuda::where('id_siswa', $request->sisid)->update([
                        'file_kk' => $NameArr1,
                    ]);
                
                /**if (count($NameArr) == null) {
                    Wisuda::where('id_siswa', $request->sisid)->update([
                        'file_kk' => ' ',
                        'file_akte' => ' ',
                        'file_bbp' => ' ', 
                        'file_bbm' => ' ', 
                        'file_ijazah' => ' ', 
                        'file_ktp' => ' ', 
                        'file_bpbw' => ' ', 
                        'file_bpktm' => ' ', 
                        'file_lpta' => ' ', 
                        'file_sppk' => ' ', 
                        'file_bplpkl' => ' ', 
                        'file_bbtp' => ' ', 
                        'file_bbta' => ' ', 
                        'file_beladiri' => ' ',
                    ]);
                }
                if ($NameArr[$ns++] == 0) {
                    Wisuda::where('id_siswa', $request->sisid)->update([
                        'file_kk' => $NameArr[0],
                    ]);
                }
                if ($NameArr[$ns++] == 1) {
                    Wisuda::where('id_siswa', $request->sisid)->update([
                        'file_akte' => $NameArr[1],
                    ]);
                }
                if (count($NameArr) == 4) {
                    Wisuda::where('id_siswa', $request->sisid)->update([
                        'file_kk' => $NameArr[0],
                        'file_akte' => $NameArr[1],
                        'file_bbp' => $NameArr[2],
                        'file_bbm' => $NameArr[3], 
                    ]);
                }
                if (count($NameArr) == 5) {
                    Wisuda::where('id_siswa', $request->sisid)->update([
                        'file_kk' => $NameArr[0],
                        'file_akte' => $NameArr[1],
                        'file_bbp' => $NameArr[2],
                        'file_bbm' => $NameArr[3],
                        'file_ktp' => $NameArr[5],  
                    ]);
                } **/


                
                
            }
            if($request->hasFile('filekk2')){
                $files2= $request->file('filekk2');
                
                $ns=0;
                    $filename2 = $files2->getClientOriginalName();
                    $foldername2 = $request->namlep;
                    $NameArr2 = $filename2;
                    $files2->move('uploads/wisudas/' . $foldername2 , $filename2);

                    Wisuda::where('id_siswa', $request->sisid)->update([
                        'file_akte' => $NameArr2,
                    ]);
            }
            if($request->hasFile('filekk3')){
                $files3= $request->file('filekk3');
                
                $ns=0;
                    $filename3 = $files3->getClientOriginalName();
                    $foldername3 = $request->namlep;
                    $NameArr3 = $filename3;
                    $files3->move('uploads/wisudas/' . $foldername3 , $filename3);

                    Wisuda::where('id_siswa', $request->sisid)->update([
                        'file_bbp' => $NameArr3,
                    ]);
            }
            if($request->hasFile('filekk4')){
                $files4= $request->file('filekk4');
                
                $ns=0;
                    $filename4 = $files4->getClientOriginalName();
                    $foldername4 = $request->namlep;
                    $NameArr4 = $filename4;
                    $files4->move('uploads/wisudas/' . $foldername4 , $filename4);

                    Wisuda::where('id_siswa', $request->sisid)->update([
                        'file_bbm' => $NameArr4,
                    ]);
            }
            if($request->hasFile('filekk5')){
                $files5= $request->file('filekk5');
                
                $ns=0;
                    $filename5 = $files5->getClientOriginalName();
                    $foldername5 = $request->namlep;
                    $NameArr5 = $filename5;
                    $files5->move('uploads/wisudas/' . $foldername5 , $filename5);

                    Wisuda::where('id_siswa', $request->sisid)->update([
                        'file_ijazah' => $NameArr5,
                    ]);
            }
            if($request->hasFile('filekk6')){
                $files6= $request->file('filekk6');
                
                $ns=0;
                    $filename6 = $files6->getClientOriginalName();
                    $foldername6 = $request->namlep;
                    $NameArr6 = $filename6;
                    $files6->move('uploads/wisudas/' . $foldername6 , $filename6);

                    Wisuda::where('id_siswa', $request->sisid)->update([
                        'file_ktp' => $NameArr6,
                    ]);
            }
            if($request->hasFile('filekk7')){
                $files7= $request->file('filekk7');
                
                $ns=0;
                    $filename7 = $files7->getClientOriginalName();
                    $foldername7 = $request->namlep;
                    $NameArr7 = $filename7;
                    $files7->move('uploads/wisudas/' . $foldername7 , $filename7);

                    Wisuda::where('id_siswa', $request->sisid)->update([
                        'file_bpbw' => $NameArr7,
                    ]);
            }
            if($request->hasFile('filekk8')){
                $files8= $request->file('filekk8');
                
                $ns=0;
                    $filename8 = $files8->getClientOriginalName();
                    $foldername8 = $request->namlep;
                    $NameArr8 = $filename8;
                    $files8->move('uploads/wisudas/' . $foldername8 , $filename8);

                    Wisuda::where('id_siswa', $request->sisid)->update([
                        'file_bpktm' => $NameArr8,
                    ]);
            }
            if($request->hasFile('filekk9')){
                $files9= $request->file('filekk9');
                
                $ns=0;
                    $filename9 = $files9->getClientOriginalName();
                    $foldername9 = $request->namlep;
                    $NameArr9 = $filename9;
                    $files9->move('uploads/wisudas/' . $foldername9 , $filename9);

                    Wisuda::where('id_siswa', $request->sisid)->update([
                        'file_lpta' => $NameArr9,
                    ]);
            }
            if($request->hasFile('filekk10')){
                $files10= $request->file('filekk10');
                
                $ns=0;
                    $filename10 = $files10->getClientOriginalName();
                    $foldername10 = $request->namlep;
                    $NameArr10 = $filename10;
                    $files10->move('uploads/wisudas/' . $foldername10 , $filename10);

                    Wisuda::where('id_siswa', $request->sisid)->update([
                        'file_sppk' => $NameArr10,
                    ]);
            }
            if($request->hasFile('filekk11')){
                $files11= $request->file('filekk11');
                
                $ns=0;
                    $filename11 = $files11->getClientOriginalName();
                    $foldername11 = $request->namlep;
                    $NameArr11 = $filename11;
                    $files11->move('uploads/wisudas/' . $foldername11 , $filename11);

                    Wisuda::where('id_siswa', $request->sisid)->update([
                        'file_bplpkl' => $NameArr11,
                    ]);
            }
            if($request->hasFile('filekk12')){
                $files12= $request->file('filekk12');
                
                $ns=0;
                    $filename12 = $files12->getClientOriginalName();
                    $foldername12 = $request->namlep;
                    $NameArr12 = $filename12;
                    $files12->move('uploads/wisudas/' . $foldername12 , $filename12);

                    Wisuda::where('id_siswa', $request->sisid)->update([
                        'file_bbtp' => $NameArr12,
                    ]);
            }
            if($request->hasFile('filekk13')){
                $files13= $request->file('filekk13');
                
                $ns=0;
                    $filename13 = $files13->getClientOriginalName();
                    $foldername13 = $request->namlep;
                    $NameArr13 = $filename13;
                    $files13->move('uploads/wisudas/' . $foldername13 , $filename13);

                    Wisuda::where('id_siswa', $request->sisid)->update([
                        'file_bbta' => $NameArr13,
                    ]);
            }
            if($request->hasFile('filekk14')){
                $files14= $request->file('filekk14');
                
                $ns=0;
                    $filename14 = $files14->getClientOriginalName();
                    $foldername14 = $request->namlep;
                    $NameArr14 = $filename14;
                    $files14->move('uploads/wisudas/' . $foldername14 , $filename14);

                    Wisuda::where('id_siswa', $request->sisid)->update([
                        'file_beladiri' => $NameArr14,
                    ]);
            }
            
            Wisuda::where('id_siswa', $request->sisid)->update([
                    'email' => $request->email,
                    'nama_lengkap' => $request->namlep,
                    'nim' => $request->nim,
                    'alamat_email' => $request->alemail,
                    'nohp' => $request->nohp,
                    'nama_ortu' => $request->namortu,
                    'prodi' => $request->prodi,
                    'tempat_lahir' => $request->temla,
                    'tanggal_lahir' => $request->tangla,
                    'alamat_domisili' => $request->alamat,
                    'nik' => $request->nik,
                    'tempat_pkl1' => $request->tempat_pkl1, 
                    'tanggal_pelaksanaan' => $request->tanggal_pelaksanaan , 
                    'judul_pkl1' => $request->judul_pkl1, 
                    'tempat_pkl2' => $request->tempat_pkl2, 
                    'tanggal_pelaksanaan2' => $request->tanggal_pelaksanaan2, 
                    'judul_pkl2' => $request->judul_pkl2, 
                    'tempat_pkl3' => $request->tempat_pkl3, 
                    'tanggal_pelaksanaan3' => $request->tanggal_pelaksanaan3, 
                    'judul_pkl3' => $request->judul_pkl3, 
                    'judul_tugas_akhir' => $request->judul_tugas_akhir, 
                    'nama_pembimbing_tugas_akhir1' => $request->nama_pembimbing_tugas_akhir1, 
                    'nama_pembimbing_tugas_akhir2' => $request->nama_pembimbing_tugas_akhir2, 
                    'nama_pembimbing_tugas_akhir3' => $request->nama_pembimbing_tugas_akhir3, 
                    'nama_penguji_tugas_akhir1' => $request->nama_penguji_tugas_akhir1, 
                    'nama_penguji_tugas_akhir2' => $request->nama_penguji_tugas_akhir2, 
                    'nama_penguji_tugas_akhir3' => $request->nama_penguji_tugas_akhir3,
                    'ipk_mahasiswa' => $request->ipk,
                ]);
            return redirect()->route('daftarwisuda');
    }

    public function storewisuda1(Request $request, $id)
    {
        
        $wisud = Wisuda::where('id_siswa', $id)->get('id_siswa')->first();

        if(  $wisud['id_siswa'] != $id ){
            $wisud = wisuda::create([
                'id_siswa' => $id,
                'email' => Auth::user()->email
            ]);
            return redirect()->route('daftarwisuda');
        }else{
            return redirect()->route('daftarwisuda');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function show(Siswa $siswa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function edit(Siswa $siswa)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Siswa $siswa)
    {
        Siswa::where('user_id', auth()->user()->id)->update([
            'nama_depan' => $request->namadep,
            'nama_belakang' => $request->namabel,
            'email' => $request->email,
            'telepon' => $request->notelp,
            'alamat' => $request->alamat,
        ]);

        User::where('id', auth()->user()->id)->update([
            'name' => $request->namadep,
        ]);
        return redirect()->route('siswa')->with('success_message', 'Deskripsi berhasil diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function destroy(Siswa $siswa)
    {
        //
    }

    public function update_avatar(Request $request)
    {
        if($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(500, 500)->save( public_path('/uploads/avatars/' . $filename) );

            $user = Auth::user();
            $user->avatar = $filename;
            $user->save();
        }
        $users = User::where('id', auth()->user()->id)->get();
        $siswa = Siswa::get();
        $siswas = Siswa::where('user_id', auth()->user()->id)->get();

        return view('siswa.SiswaLanding', compact('users', 'siswa', 'siswas'), array('user' => Auth::user()));

    }
}
