<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use PDF;
use Auth;
use App\User;
use App\Siswa;
use App\Wisuda;
use App\Berita;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $berita = Berita::get();

        return view('home/landing', compact('berita'));
    }
    public function index2()
    {
        $berita = Berita::get();

        return view('home/reset', compact('berita'));   
    }
    //controller admin start

    public function userview()
    {
        $users = User::get();
        $siswas = Siswa::get();
        return view('admin/UserRecord', compact('users', 'siswas'));
    }

    public function bukualumniview()
    {
        $users = User::get();
        $siswas = Siswa::get();
        $wisuda = Wisuda::orderBy('ipk_mahasiswa', 'DESC')->get();

        return view('admin/BukuAlumniRecord', compact('users', 'siswas', 'wisuda'));
    }
    public function cetakbukualumni()
    {
        $users = User::get();
        $wisuda = Wisuda::orderBy('ipk_mahasiswa', 'DESC')->get();
        $siswas = Siswa::get();
        $pdf = PDF::loadview('admin/BukuAlumni_pdf',['wisuda'=>$wisuda, 'siswas'=>$siswas]);
        $pdf->setPaper('A4');
        return $pdf->download('laporan-BukuAlumni-pdf');
    }

    public function wisudaview()
    {
        $users = User::get();

        $siswas = Siswa::get();
        $wisuda = Wisuda::get();
        return view('admin/WisudaRecord', compact('users', 'siswas', 'wisuda'));
    }

    public function beritaview()
    {
        $users = User::get();

        $siswas = Siswa::get();
        $berita = Berita::get();
        $nil = 1;
        return view('admin/BeritaRecord', compact('users', 'siswas', 'berita', 'nil'));
    }

    public function tambahberitaview()
    {
        $users = User::get();

        $siswas = Siswa::get();
        return view('admin/TambahBerita', compact('users', 'siswas'));
    }
    //controller admin end


    //controller admin super start
    public function userviewsuper()
    {
        $users = User::get();

        $siswas = Siswa::get();
        return view('adminsuper/UserRecord', compact('users', 'siswas'));
    }

    public function wisudaviewsuper()
    {
        $users = User::get();

        $siswas = Siswa::get();
        $wisuda = Wisuda::get();
        return view('adminsuper/WisudaRecord', compact('users', 'siswas', 'wisuda'));
    }

    public function beritaviewsuper()
    {
        $users = User::get();

        $siswas = Siswa::get();
        $berita = Berita::get();
        $nil = 1;
        return view('adminsuper/BeritaRecord', compact('users', 'siswas', 'berita', 'nil'));
    }

    public function tambahberitaviewsuper()
    {
        $users = User::get();

        $siswas = Siswa::get();
        return view('adminsuper/TambahBerita', compact('users', 'siswas'));
    }

    //controller admin super end

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    //controller admin start
    public function edituser(Request $request, $id)
    {
        $userid = $request->iduser;
        $users = User::get();
        $siswas = Siswa::where('user_id', $userid)->get();
        return view('admin/EditUser', compact('users', 'id', 'siswas'));
    }

    public function editwisuda(Request $request, $id)
    {
        $wisudaid = $request->idsiswa;
        $wisuda = Wisuda::where('id_siswa', $wisudaid)->get();
        $siswas = Siswa::where('id', $wisudaid)->get();
        $users = User::get();
        return view('admin/EditWisuda', compact('wisuda', 'id', 'siswas', 'users'));
    }
    public function editwisudacetak(Request $request, $id)
    {
        $wisudaid = $request->idsiswa;
        $wisuda = Wisuda::where('id_siswa', $wisudaid)->get();
        $wisudasa = Wisuda::where('id_siswa', $wisudaid)->get('nim')->first();
        $siswas = Siswa::where('id', $wisudaid)->get();
        $users = User::get();
        $pdf = PDF::loadview('admin/WisudaCetak_pdf',['wisuda'=>$wisuda, 'siswas'=>$siswas, 'users'=>$users, 'id'=>$id]);
        $pdf->setPaper('A4');
        return $pdf->download('DataMahasiswa-'.$wisudasa['nim'].'.pdf');
    }

    public function editberita(Request $request, $id)
    {
        $userid = $request->idberita;
        $users = User::get();
        $siswas = Siswa::where('user_id', $userid)->get();
        $berita = berita::where('id', $userid)->get();
        return view('admin/EditBerita', compact('users', 'id', 'berita', 'siswas'));
    }

    public function tambahberita(Request $request)
    {
        $berita = new Berita;
        $berita->user_id = auth()->user()->id;
        $berita->berita = $request->berita;
        $berita->save();
        return redirect()->route('viewberita')->with('success_message', 'Deskripsi berhasil diperbarui');
    }

    //controller admin end

    //controller admin super start
    public function editusersuper(Request $request, $id)
    {
        $userid = $request->iduser;
        $users = User::get();
        $siswas = Siswa::where('user_id', $userid)->get();
        return view('adminsuper/EditUser', compact('users', 'id', 'siswas'));
    }

    public function editwisudasuper(Request $request, $id)
    {
        $wisudaid = $request->idsiswa;
        $wisuda = Wisuda::where('id_siswa', $wisudaid)->get();
        $siswas = Siswa::where('id', $wisudaid)->get();
        $users = User::get();
        return view('adminsuper/EditWisuda', compact('wisuda', 'id', 'siswas', 'users'));
    }

    public function editberitasuper(Request $request, $id)
    {
        $userid = $request->idberita;
        $users = User::get();
        $siswas = Siswa::where('user_id', $userid)->get();
        $berita = berita::where('id', $userid)->get();
        return view('adminsuper/EditBerita', compact('users', 'id', 'berita', 'siswas'));
    }

    public function tambahberitasuper(Request $request)
    {
        $berita = new Berita;
        $berita->user_id = auth()->user()->id;
        $berita->berita = $request->berita;
        $berita->save();
        return redirect()->route('viewberitasuper')->with('success_message', 'Deskripsi berhasil diperbarui');
    }

    //controller admin super end

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    //controller admin start

    public function update(Request $request, User $User)
    {
        $userid = $request->iduser;
        User::where('id', $userid)->update([
            'name' => $request->username,
            'email' => $request->email,
        ]);

        Siswa::where('user_id', $userid)->update([
            'status' => $request->stat,
        ]);
        return redirect()->route('viewuser')->with('success_message', 'Deskripsi berhasil diperbarui');
    }

    public function updatewisuda(Request $request, User $User)
    {
        $userid = $request->sisid;
        Siswa::where('id', $userid)->update([
            'status' => $request->stat,
        ]);
        Wisuda::where('id_siswa', $userid)->update([
            'komentar' => $request->komentar,
        ]);
        return redirect()->route('viewwisuda')->with('success_message', 'Deskripsi berhasil diperbarui');
    }

    public function updateberita(Request $request, User $User)
    {
        $userid = $request->berid;
        Berita::where('id', $userid)->update([
            'berita' => $request->berita,
        ]);
        return redirect()->route('viewberita')->with('success_message', 'Deskripsi berhasil diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function deleteberita($id)
    {
        Berita::where('id',$id)->delete();
        return redirect()->route('viewberita')->with('success_message', 'Deskripsi berhasil dihapus');
    }

    //controller admin end

    //controller admin super start

    public function updatesuper(Request $request, User $User)
    {
        $userid = $request->iduser;
        User::where('id', $userid)->update([
            'name' => $request->username,
            'email' => $request->email,
            'status' => $request->status,
        ]);

        Siswa::where('user_id', $userid)->update([
            'status' => $request->stat,
        ]);
        return redirect()->route('viewusersuper')->with('success_message', 'Deskripsi berhasil diperbarui');
    }

    public function updatewisudasuper(Request $request, User $User)
    {
        $userid = $request->sisid;
        Siswa::where('id', $userid)->update([
            'status' => $request->stat,
        ]);
        Wisuda::where('id_siswa', $userid)->update([
            'komentar' => $request->komentar,
        ]);
        return redirect()->route('viewwisudasuper')->with('success_message', 'Deskripsi berhasil diperbarui');
    }

    public function updateberitasuper(Request $request, User $User)
    {
        $userid = $request->berid;
        Berita::where('id', $userid)->update([
            'berita' => $request->berita,
        ]);
        return redirect()->route('viewberitasuper')->with('success_message', 'Deskripsi berhasil diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function deleteberitasuper($id)
    {
        Berita::where('id',$id)->delete();
        return redirect()->route('viewberitasuper')->with('success_message', 'Deskripsi berhasil dihapus');
    }

    //controller admin super end

    public function destroy($id)
    {
        //
    }
}
